package by.bsu.khrapko.parser;

import by.bsu.khrapko.composite.*;
import by.bsu.khrapko.creator.FileCreator;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 14.12.2015.
 */
public class TextParser {

    static Logger logger = Logger.getLogger(TextParser.class);

    private final static String PARAGRAPH_REGEX = "(\\p{Blank}{4})";
    private final static String LISTING_START = "(\\p{Blank}{4}//start)";
    private final static String LISTING_END = "(\\p{Blank}{4}//end)";
    private final static String SENTENCE_REGEX = "([.!?])+(\\p{Blank})+";
    private final static String LAST_SENTENCE_REGEX = "(.*)([.?!])+";
    private final static String LEXEM_REGEX = "(\\p{Blank})";
    private final static String LEXEM_PUNCT_REGEX = "[^:;,]+";
    private final static String LEXEM_EXPRESSION_REGEX = "([\\[(<]+)([^)>(<]+)([>)\\]]+)";
    private final static String LEXEM_PREFIX_REGEX = "([\\[(/{*]+)([^(/*{]+)";
    private final static String LEXEM_POSTFIX_REGEX = "([^/*})]+)([*/})\\]]+)";
    private final static String LEXEM_METHOD_REGEX = "([^(]+)([(])";

    public Composite createComposite(String firstpath, String morepath){
        Composite composite = new Composite();
        FileCreator fileCreator = new FileCreator();
        String text = fileCreator.readFromFile(firstpath, morepath);
        textParser(composite, text);
        return composite;
    }

    private void textParser(Composite composite, String text){
        Pattern pattern1 = Pattern.compile(LISTING_START);
        Pattern pattern2 = Pattern.compile(LISTING_END);
        Matcher matcher1 = pattern1.matcher(text);
        Matcher matcher2 = pattern2.matcher(text);
        int startFindIndex = 0;
        while(matcher1.find(startFindIndex)){
            composite.add(new Composite(Including.TEXT));
            paragraphParser((Composite) composite.getChild(composite.size()-1), text.substring(startFindIndex, matcher1.start(1)));
            startFindIndex = matcher1.end(1);
            matcher2.find(startFindIndex);
            composite.add(new ListingLeaf(text.substring(startFindIndex, matcher2.start(1))));
            startFindIndex = matcher2.end(1);
        }
        if (startFindIndex != text.length()){
            composite.add(new Composite(Including.TEXT));
            paragraphParser((Composite) composite.getChild(composite.size()-1), text.substring(startFindIndex, text.length()));
        }
    }

    private void paragraphParser(Composite composite, String text){
        Pattern pattern = Pattern.compile(PARAGRAPH_REGEX);
        Matcher matcher = pattern.matcher(text);
        int startFindIndex = 4;
        while(matcher.find(startFindIndex)){
            composite.add(new Composite(Including.PARAGRAPH));
            //logger.debug(matcher.group(2));
            sentenceParser((Composite)composite.getChild(composite.size()-1), text.substring(startFindIndex, matcher.start()));
            startFindIndex = matcher.end();
        }
        if (startFindIndex != text.length()){
            composite.add(new Composite(Including.PARAGRAPH));
            sentenceParser((Composite) composite.getChild(composite.size()-1), text.substring(startFindIndex, text.length()));
        }
    }

    private void sentenceParser(Composite composite, String text){
        Pattern pattern = Pattern.compile(SENTENCE_REGEX);
        Matcher matcher = pattern.matcher(text);
        int startFindIndex = 0;
        while(matcher.find(startFindIndex)){
            composite.add(new Composite(Including.SENTENCE));
            lexemParser((Composite) composite.getChild(composite.size()-1), text.substring(startFindIndex, matcher.start(1)));
            //logger.debug(text.substring(startFindIndex, matcher.start(1)));
            for (int i=0; i<matcher.group(1).length(); i++){
                composite.add(new PunctuationLeaf(matcher.group(1).charAt(i)));
            }
            startFindIndex = matcher.end();
        }
        if (startFindIndex != text.length()){
            pattern = Pattern.compile(LAST_SENTENCE_REGEX);
            matcher = pattern.matcher(text.substring(startFindIndex, text.length()));
            if(matcher.find()) {
                //logger.debug(matcher.group(1));
                composite.add(new Composite(Including.SENTENCE));
                lexemParser((Composite) composite.getChild(composite.size() - 1), matcher.group(1));
                for (int i = 0; i < matcher.group(2).length(); i++) {
                    composite.add(new PunctuationLeaf(matcher.group(2).charAt(i)));
                }
            }else{
                composite.add(new Composite(Including.SENTENCE));
                lexemParser((Composite) composite.getChild(composite.size() - 1), text.substring(startFindIndex, text.length()));
            }
        }
    }

    private void lexemParser(Composite composite, String text){
        String[] lexemArray = text.split(LEXEM_REGEX);
        Pattern pattern = Pattern.compile(LEXEM_PUNCT_REGEX);
        Matcher matcher;
        for (int i=0; i<lexemArray.length; i++){
            matcher = pattern.matcher(lexemArray[i]);
            if (matcher.find()){
                lexemExpressionParser(composite, lexemArray[i].substring(0, matcher.end()));
                for (int j=matcher.end(); j < lexemArray[i].length(); j++){
                    composite.add(new PunctuationLeaf(lexemArray[i].charAt(j)));
                }
            }
        }
    }

    private void lexemExpressionParser(Composite composite, String text){
        Pattern pattern = Pattern.compile(LEXEM_EXPRESSION_REGEX);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()){
            //logger.debug(matcher.start(2));
            for (int i=0; i<matcher.start(2); i++){
                composite.add(new PunctuationLeaf(text.charAt(i)));
            }
            composite.add(new WordLeaf(matcher.group(2)));
            for (int i=matcher.start(3); i<text.length(); i++){
                composite.add(new PunctuationLeaf(text.charAt(i)));
            }
        }else {
            lexemPrefixPostfixParser(composite, text);
        }
    }

    private void lexemPrefixPostfixParser(Composite composite, String text){
        Pattern pattern = Pattern.compile(LEXEM_PREFIX_REGEX);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()){
            // префикс+буквы
            for (int i=0; i<matcher.start(2); i++){
                composite.add(new PunctuationLeaf(text.charAt(i)));
            }
            composite.add(new WordLeaf(matcher.group(2)));
        }else {
            pattern = Pattern.compile(LEXEM_POSTFIX_REGEX);
            matcher = pattern.matcher(text);
            if (matcher.matches()) {
                //проверка на метод ()
                Pattern pattern1 = Pattern.compile(LEXEM_METHOD_REGEX);
                Matcher matcher1 = pattern1.matcher(matcher.group(1));
                if (matcher.group(2).equals(")") && matcher1.matches()){
                    composite.add(new WordLeaf(text));
                }else {
                    // не метод, но удовлетворяет буквы+постфикс
                    composite.add(new WordLeaf(matcher.group(1)));
                    for (int i = matcher.start(2); i < text.length(); i++) {
                        composite.add(new PunctuationLeaf(text.charAt(i)));
                    }
                }
            }else{
                composite.add(new WordLeaf(text));
            }
        }
    }
}