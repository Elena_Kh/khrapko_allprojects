package by.bsu.khrapko.main;

import by.bsu.khrapko.parser.TextParser;
import by.bsu.khrapko.composite.Component;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by user on 14.12.2015.
 */
public class Solution {

    static{
        new DOMConfigurator().doConfigure("logs/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Solution.class);

    public static void main(String[] args) {
        TextParser textParser = new TextParser();
        Component composite = textParser.createComposite("input", "text.txt");
        composite.operation();
    }
}
