package by.bsu.khrapko.composite;

/**
 * Created by user on 14.12.2015.
 */
public class PunctuationLeaf implements Component {

    private char c;

    public PunctuationLeaf(char c) {
        this.c = c;
    }

    @Override
    public void operation() {
        System.out.print(c);
    }

    @Override
    public Object getChild(int index) {
        throw new UnsupportedOperationException();
    }
}
