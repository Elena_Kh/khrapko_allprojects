package by.bsu.khrapko.composite;

/**
 * Created by user on 14.12.2015.
 */
public class ListingLeaf implements Component {

    private String str;

    public ListingLeaf(String str){
        this.str = str;
    }

    @Override
    public void operation() {
        System.out.print("    //start\n");
        System.out.print("    " + str + "\n");
        System.out.print("    //end\n");
    }

    @Override
    public Object getChild(int index) {
        throw new UnsupportedOperationException();//return null;
    }
}
