package by.bsu.khrapko.composite;

import java.util.Arrays;

/**
 * Created by user on 14.12.2015.
 */
public class WordLeaf implements Component{

    private String str;

    public WordLeaf(String str){
        this.str = str;
    }

    @Override
    public void operation() {
        this.str = removeLetters();
        System.out.print(" " + str);
    }

    @Override
    public Object getChild(int index) {
        throw new UnsupportedOperationException();//return null;
    }

    public String removeLetters(){
        String lowerStr = str.toLowerCase();
        char[] charArray = lowerStr.toCharArray();
        for (int i=1; i<str.length(); i++){
            if (charArray[i] == charArray[0]){
                charArray[i] = ' ';
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str.charAt(0));
        for (int i=1; i<charArray.length; i++){
            if (charArray[i] != ' '){
                stringBuilder.append(charArray[i]);
            }
        }
        return stringBuilder.toString();
    }
}
