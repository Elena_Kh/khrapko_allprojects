package by.bsu.khrapko.composite;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by user on 14.12.2015.
 */
public class Composite implements Component {
    private ArrayList<Component> components = new ArrayList<Component>();
    private Including including;

    public Composite(){

    }

    public Composite(Including including){
        this.including = including;
    }

    public void operation() {
        if (including == Including.PARAGRAPH){
            System.out.print("   ");
        }
        if (including == Including.SENTENCE){
            swapWords();
        }
        for (int i = 0; i < components.size(); i++) {
            components.get(i).operation();
        }
        if (including == Including.PARAGRAPH){
            System.out.print("\n");
        }
    }
    public void add(Component component) {
        components.add(component);
    }

    public Object getChild(int index) {
        return components.get(index);
    }

    public int size(){
        return components.size();
    }

    private void swapWords(){
        int firstIndex = 0, lastIndex = components.size()-1;
        while (!(components.get(firstIndex) instanceof WordLeaf)){
            firstIndex++;
        }
        while (!(components.get(lastIndex) instanceof WordLeaf)){
            lastIndex--;
        }
        if(firstIndex != lastIndex){
            Collections.swap(components, firstIndex, lastIndex);
        }
    }
}
