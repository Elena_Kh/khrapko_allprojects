package by.bsu.khrapko.composite;

/**
 * Created by user on 14.12.2015.
 */
public interface Component {
    void operation();
    Object getChild (int index);
}
