package by.bsu.khrapko.creator;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public interface FileHandler {


    /**
     * Parameters equivalent to parameters in FileSystems.getDefault().getPath(String first, String more)
     * @param firstPath
     * @param morePath
     * @return list which elements are file lines
     */

    static Logger logger = Logger.getLogger(FileHandler.class);

    static List<String> linesList(String firstPath, String... morePath)  {

        Path path = FileSystems.getDefault().getPath(firstPath, morePath);
        Stream<String> lines = null;
        try {
            lines = Files.lines(path);
        } catch (IOException e) {
            logger.fatal("File is not found");
            //e.printStackTrace();
        }
        List<String> listNames = new ArrayList<String>();
        if (lines != null) {
            lines.forEach(fioTmp -> listNames.add(fioTmp));
        }
        return listNames;
    }
}
