package by.bsu.khrapko.creator;

import java.util.List;

/**
 * Created by user on 16.12.2015.
 */
public class FileCreator implements FileHandler {

    public String readFromFile(String firstpath, String morepath){
        StringBuilder str = new StringBuilder();
        List<String> list = FileHandler.linesList(firstpath, morepath);
        for (int i=0; i<list.size(); i++){
            str.append(list.get(i));
        }
        return str.toString();
    }
}
