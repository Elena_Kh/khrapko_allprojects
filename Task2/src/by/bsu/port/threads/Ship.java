package by.bsu.port.threads;

import by.bsu.port.entity.Moorage;
import by.bsu.port.entity.Port;
import by.bsu.port.exception.ResourсeException;
import org.apache.log4j.Logger;

/**
 * Created by user on 06.12.2015.
 */
public class Ship extends Thread {
    private int numberOfContainers;
    public final int MAX_NUMBER_OF_CONTAINERS;
    private Port port;

    static Logger logger = Logger.getLogger(Ship.class);

    public Ship(int numberOfContainers, int maxNumberOfContainers, Port port){
        super();
        this.numberOfContainers = numberOfContainers;
        MAX_NUMBER_OF_CONTAINERS = maxNumberOfContainers;
        this.port = port;
    }

    public Port getPort() {
        return port;
    }

    public int getNumberOfContainers() {
        return numberOfContainers;
    }

    public void setNumberOfContainers(int numberOfContainers) {
        this.numberOfContainers = numberOfContainers;
    }

    @Override
    public void run() {
        try {
            Moorage moorage;
            do {
                moorage = port.takeResource(500);
            } while (moorage == null);
            if (numberOfContainers == 0){
                moorage.loading(this);
            }else {
                moorage.uploading(this);
            }
            port.returnResource(moorage);
        } catch (ResourсeException e){
            logger.warn("Ship has problems.", e);
        }
    }

}
