package by.bsu.port.main;

import by.bsu.port.creator.ShipCreator;
import by.bsu.port.entity.Port;
import by.bsu.port.threads.Ship;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;

/**
 * Created by user on 07.12.2015.
 */
public class Solution {

    static{
        new DOMConfigurator().doConfigure("logs/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Solution.class);

    public static void main(String[] args){
        Port port = Port.getInstance();
        ArrayList<Ship> shipArrayList = new ShipCreator().createShip("input/ship.txt", port);
        new ShipCreator().startShip(shipArrayList);
    }
}
