package by.bsu.port.exception;

/**
 * Created by user on 07.12.2015.
 */
public class ResourсeException extends Exception {
    public ResourсeException() {
    }

    public ResourсeException(String message) {
        super(message);
    }

    public ResourсeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourсeException(Throwable cause) {
        super(cause);
    }

}
