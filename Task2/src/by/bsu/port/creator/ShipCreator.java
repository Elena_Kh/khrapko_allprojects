package by.bsu.port.creator;

import by.bsu.port.entity.Port;
import by.bsu.port.threads.Ship;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 07.12.2015.
 */
public class ShipCreator {

    static Logger logger = Logger.getLogger(ShipCreator.class);

    public ArrayList<Ship> createShip(String filename, Port port){
        ArrayList<Ship> shipArrayList = new ArrayList<Ship>();
        Ship ship;
        try {
            Scanner scan = new Scanner(new File(filename));
            while (scan.hasNextLine()){
                String str = scan.nextLine();
                String[] tokens = str.split(" ");
                ship = new Ship(Integer.parseInt(tokens[0]),
                        Integer.parseInt(tokens[1]), port);
                shipArrayList.add(ship);
                //logger.debug("Корабль " + ship.getShipID() + " создан");
            }
        } catch (FileNotFoundException e) {
            logger.error("File is not found!", e);
        }
        return shipArrayList;
    }

    public void startShip(ArrayList<Ship> shipArrayList){
        for (int i=0; i<shipArrayList.size(); i++){
            shipArrayList.get(i).start();
            //logger.debug("Корабль " + shipArrayList.get(i).getShipID() + " запущен");
        }
    }
}
