package by.bsu.port.entity;

import by.bsu.port.exception.ResourсeException;
import by.bsu.port.threads.Ship;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Created by user on 07.12.2015.
 */
public class Moorage {
    private int moorageID;
    private final int TIME_FOR_1_CONTAINER = 25;

    static Logger logger = Logger.getLogger(Moorage.class);

    public Moorage(int id){
        moorageID = id;
    }

    public void uploading(Ship ship) throws ResourсeException {
        logger.debug("Ship " + Thread.currentThread().getName() + " came to upload at moorage " + this.getMoorageID());
        while (ship.getNumberOfContainers() != 0){
            if (Port.getPortCurrentCapacity().get() == ship.getPort().getPortCapacity()){
                ship.getPort().callingTruck();
            }else if (Port.getPortCurrentCapacity().get() == 0 && Port.getWaitingNumber() < 0){
                Port.setWaitingNumber(0);
            }
            int numberOfUploadedContainers = Math.min(ship.getNumberOfContainers(),
                    ship.getPort().getPortCapacity() - Port.getPortCurrentCapacity().get());
            logger.debug("Ship " + Thread.currentThread().getName() + " uploaded "
                    + numberOfUploadedContainers + " containers");
            Port.getPortCurrentCapacity().addAndGet(numberOfUploadedContainers);
            logger.debug("Current port capacity: " + Port.getPortCurrentCapacity().get());
            ship.setNumberOfContainers(ship.getNumberOfContainers() - numberOfUploadedContainers);
            try {
                TimeUnit.MILLISECONDS.sleep(TIME_FOR_1_CONTAINER*numberOfUploadedContainers);
                if (ship.getNumberOfContainers() != 0) {
                    TimeUnit.MILLISECONDS.sleep(500);
                }
            } catch (InterruptedException e) {
                logger.warn("Ship is not correct", e);//throw new ResourсeException();
            }
        }
    }

    public void loading(Ship ship) throws ResourсeException {
        logger.debug("Ship " + Thread.currentThread().getName() + " came to load at moorage " + this.getMoorageID());
        while (ship.getNumberOfContainers() != ship.MAX_NUMBER_OF_CONTAINERS){
            if (Port.getPortCurrentCapacity().get() == 0){
                ship.getPort().callingTruck();
            }else if (Port.getPortCurrentCapacity().get() == ship.getPort().getPortCapacity() &&
                    Port.getWaitingNumber() > 0){
                Port.setWaitingNumber(0);
            }
            int numberOfLoadedContainers = Math.min(ship.MAX_NUMBER_OF_CONTAINERS-ship.getNumberOfContainers(),
                    Port.getPortCurrentCapacity().get());
            logger.debug("Ship " + Thread.currentThread().getName() + " loaded "
                    + numberOfLoadedContainers + " containers");
            Port.getPortCurrentCapacity().addAndGet(-1*numberOfLoadedContainers);
            logger.debug("Current port capacity: " + Port.getPortCurrentCapacity().get());
            ship.setNumberOfContainers(ship.getNumberOfContainers() + numberOfLoadedContainers);
            try {
                TimeUnit.MILLISECONDS.sleep(TIME_FOR_1_CONTAINER*numberOfLoadedContainers);
                if (ship.getNumberOfContainers() != ship.MAX_NUMBER_OF_CONTAINERS) {
                    TimeUnit.MILLISECONDS.sleep(500);
                }
            } catch (InterruptedException e) {
                logger.warn("Ship is not correct", e);//throw new ResourсeException();
            }
        }
    }

    public int getMoorageID() {
        return moorageID;
    }
}
