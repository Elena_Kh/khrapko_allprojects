package by.bsu.port.entity;
import by.bsu.port.exception.ResourсeException;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by user on 07.12.2015.
 */
public class Port{

    static Logger logger = Logger.getLogger(Port.class);
    private static Port instance;
    private static AtomicInteger portCurrentCapacity = new AtomicInteger(0);
    private static int waitingNumber;
    private static final int PORT_CAPACITY = 100;
    private static final int MAX_AVAILABLE_MOORAGE = 10; // лимит экземпляров списка
    private static final int TRUCK = 20;
    private static Queue<Moorage> moorages = new LinkedList<Moorage>();
    private static Lock lock = new ReentrantLock();

    private Port(){
        init(moorages, MAX_AVAILABLE_MOORAGE);
    }

    public static Port getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new Port();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    private void init(Queue<Moorage> moorages, final int MAX_AVAILABLE_MOORAGE){
        for (int i=0; i<MAX_AVAILABLE_MOORAGE; i++){
            moorages.add(new Moorage(i+1));
        }
        return;
    }

    public Moorage takeResource(int maxWaitTime) throws ResourсeException {
        //logger.debug("Ship " + Thread.currentThread().getName() + " want to take moorage");
        try {
            if (lock.tryLock(maxWaitTime, TimeUnit.MILLISECONDS)) {
                try {
                    if (!moorages.isEmpty()) {
                        logger.debug("Ship " + Thread.currentThread().getName() + " reseived moorage ");
                        //logger.debug("The number of free moorages is " + (moorages.size() - 1));
                        return (Moorage) moorages.poll();
                    }
                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            throw new ResourсeException("No moorage!");
        }
        return null;
    }

    public void returnResource(Moorage moorage) {
        try{
            lock.lock();
            moorages.add(moorage);
            logger.debug("Ship " + Thread.currentThread().getName() + " return moorage " + moorage.getMoorageID());
        }
        finally {
            lock.unlock();
        }
    }

    public void callingTruck(){
        if (portCurrentCapacity.get() == PORT_CAPACITY){
            waitingNumber++;
        }else if (portCurrentCapacity.get() == 0){
            waitingNumber--;
        }
        if (waitingNumber == MAX_AVAILABLE_MOORAGE - moorages.size()){
            logger.debug("Port storage is uploaded from land");
            portCurrentCapacity.addAndGet(-TRUCK);
            logger.debug("Current port capacity: " + portCurrentCapacity.get());
            waitingNumber = 0;
        }else if (waitingNumber + MAX_AVAILABLE_MOORAGE - moorages.size() == 0){
            logger.debug("Port storage is loaded from land");
            portCurrentCapacity.addAndGet(TRUCK);
            logger.debug("Current port capacity: " + portCurrentCapacity.get());
            waitingNumber = 0;
        }
    }

    public static AtomicInteger getPortCurrentCapacity() {
        return portCurrentCapacity;
    }

    public static int getPortCapacity() {
        return PORT_CAPACITY;
    }

    public static int getWaitingNumber() {
        return waitingNumber;
    }

    public static void setWaitingNumber(int waitingNumber) {
        Port.waitingNumber = waitingNumber;
    }
}

