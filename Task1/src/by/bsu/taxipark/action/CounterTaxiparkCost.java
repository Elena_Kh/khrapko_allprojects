package by.bsu.taxipark.action;

import by.bsu.taxipark.entity.Auto;
import by.bsu.taxipark.exception.EmptyContainerException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by user on 25.11.2015.
 */
public class CounterTaxiparkCost {
    static Logger logger = Logger.getLogger(CounterTaxiparkCost.class);

    public int taxiparkValue(ArrayList<Auto> taxipark){
        if (taxipark.isEmpty()){
            logger.error("Taxipark is empty!", new EmptyContainerException());
        }
        int value = 0;
        for (Auto elem:taxipark) {
            value += elem.getCost();
        }
        return value;
    }
}
