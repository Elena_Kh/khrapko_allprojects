package by.bsu.taxipark.action;

import by.bsu.taxipark.entity.*;
import by.bsu.taxipark.exception.EmptyContainerException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by user on 24.11.2015.
 */
public class SearcherAuto {

    static Logger logger = Logger.getLogger(SearcherAuto.class);
    public static final Double MIN_LIFTING_CAPACITY = 1.0;
    public static final Double MAX_LIFTING_CAPACITY = 5.0;
    public static final Integer MIN_NUMBER_OF_SEATS = 5;
    public static final Integer MAX_NUMBER_OF_SEATS = 10;

    public String findAuto(ArrayList<Auto> taxipark){
        if (taxipark.isEmpty()){
            logger.error("Taxipark is empty!", new EmptyContainerException());
        }
        StringBuilder str = new StringBuilder();
        for (Auto a:taxipark){
            if (a.getLiftingCapasity() >= MIN_LIFTING_CAPACITY && a.getLiftingCapasity() <= MAX_LIFTING_CAPACITY &&
                    a.getNumberOfSeats() >= MIN_NUMBER_OF_SEATS && a.getNumberOfSeats() <= MAX_NUMBER_OF_SEATS){
                str.append(a.toString() + "\n");
            }
        }
        if (str.length() != 0){
            return new String(str);
        }
        else{
            logger.debug("Auto in this range doesn't exist");
            return "Doesn't exist!";
        }

    }
}
