package by.bsu.taxipark.main;
import by.bsu.taxipark.action.CounterTaxiparkCost;
import by.bsu.taxipark.action.SearcherAuto;
import by.bsu.taxipark.creator.CargoTaxiCreator;
import by.bsu.taxipark.creator.MinibusCreator;
import by.bsu.taxipark.creator.TaxiCreator;
import by.bsu.taxipark.entity.Auto;
import by.bsu.taxipark.report.PrinterTaxipark;
import by.bsu.taxipark.util.FuelComparator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by user on 24.11.2015.
 */
public class Solution {

    static{
        new DOMConfigurator().doConfigure("logs/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Solution.class);

    public static void main(String[] args){
        ArrayList<Auto> taxipark = new ArrayList<Auto>();
        taxipark.addAll(new TaxiCreator().createTaxi("input/taxi.txt"));
        taxipark.addAll(new MinibusCreator().createMinibus("input/minibus.txt"));
        taxipark.addAll(new CargoTaxiCreator().createCargoTaxi("input/cargotaxi.txt"));
        PrinterTaxipark printer = new PrinterTaxipark();
        printer.printTaxipark(taxipark);
        System.out.println("Taxipark cost: " + new CounterTaxiparkCost().taxiparkValue(taxipark));
        Collections.sort(taxipark, new FuelComparator());
        printer.printTaxipark(taxipark);
        System.out.println(new SearcherAuto().findAuto(taxipark));
        logger.info("ok");
    }
}
