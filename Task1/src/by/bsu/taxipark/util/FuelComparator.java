package by.bsu.taxipark.util;

import by.bsu.taxipark.entity.Auto;

import java.util.Comparator;

/**
 * Created by user on 24.11.2015.
 */
public class FuelComparator implements Comparator<Auto> {

    private final int ROUND = 100;
    @Override
    public int compare(Auto a1, Auto a2) {
        return (int) (ROUND*(a1.getFuelConsumption()- a2.getFuelConsumption()));
    }
}
