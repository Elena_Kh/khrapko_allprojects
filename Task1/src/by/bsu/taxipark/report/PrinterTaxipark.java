package by.bsu.taxipark.report;

import by.bsu.taxipark.entity.Auto;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Created by user on 25.11.2015.
 */
public class PrinterTaxipark {

    static Logger logger = Logger.getLogger(PrinterTaxipark.class);

    public void printTaxipark(ArrayList<Auto> taxipark){
        StringBuilder str = new StringBuilder("Taxipark:\n");
        ListIterator<Auto> it = taxipark.listIterator();
        while (it.hasNext()){
            str.append(it.next().toString());
            str.append("\n");
        }
        System.out.println(str);
    }
}
