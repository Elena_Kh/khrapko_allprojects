package by.bsu.taxipark.creator;

import by.bsu.taxipark.entity.Taxi;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 25.11.2015.
 */
public class TaxiCreator {

    static Logger logger = Logger.getLogger(TaxiCreator.class);

    public ArrayList<Taxi> createTaxi(String filename){
        ArrayList<Taxi> taxiArrayList = new ArrayList<Taxi>();
        Taxi taxi;
        try {
            Scanner scan = new Scanner(new File(filename));
            while (scan.hasNextLine()){
                String str = scan.nextLine();
                String[] tokens = str.split(" ");
                taxi = new Taxi(tokens[0], Integer.parseInt(tokens[1]),
                        Integer.parseInt(tokens[2]), Double.parseDouble(tokens[3]),
                        Double.parseDouble(tokens[4]), tokens[5]);
                /*taxi.setMakeOfTheCar(tokens[0]);
                taxi.setCost(Integer.parseInt(tokens[1]));
                taxi.setNumberOfSeats(Integer.parseInt(tokens[2]));
                taxi.setFuelConsumption(Double.parseDouble(tokens[3]));
                taxi.setLiftingCapasity(Double.parseDouble(tokens[4]));
                taxi.setVehicleClass(tokens[5]);*/
                taxiArrayList.add(taxi);
            }
        } catch (FileNotFoundException e) {
            logger.error("File is not found!", e);
        }
        return taxiArrayList;
    }
}
