package by.bsu.taxipark.creator;

import by.bsu.taxipark.entity.CargoTaxi;
import by.bsu.taxipark.entity.Minibus;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 25.11.2015.
 */
public class MinibusCreator {

    static Logger logger = Logger.getLogger(MinibusCreator.class);

    public ArrayList<Minibus> createMinibus(String filename){
        ArrayList<Minibus> minibusArrayList = new ArrayList<Minibus>();
        Minibus minibus;
        try {
            Scanner scan = new Scanner(new File(filename));
            while (scan.hasNextLine()){
                String str = scan.nextLine();
                String[] tokens = str.split(" ");
                minibus = new Minibus(tokens[0], Integer.parseInt(tokens[1]),
                        Integer.parseInt(tokens[2]), Double.parseDouble(tokens[3]),
                        Double.parseDouble(tokens[4]), Integer.parseInt(tokens[5]));
                /*minibus.setMakeOfTheCar(tokens[0]);
                minibus.setCost(Integer.parseInt(tokens[1]));
                minibus.setNumberOfSeats(Integer.parseInt(tokens[2]));
                minibus.setFuelConsumption(Double.parseDouble(tokens[3]));
                minibus.setLiftingCapasity(Double.parseDouble(tokens[4]));
                minibus.setMaxNumberOfPassengers(Integer.parseInt(tokens[5]));*/
                minibusArrayList.add(minibus);
            }
        } catch (FileNotFoundException e) {
            logger.error("File is not found!", e);
        }
        return minibusArrayList;
    }
}
