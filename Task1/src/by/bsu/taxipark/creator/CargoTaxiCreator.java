package by.bsu.taxipark.creator;

import by.bsu.taxipark.entity.CargoTaxi;
import by.bsu.taxipark.entity.Taxi;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 25.11.2015.
 */
public class CargoTaxiCreator {

    static Logger logger = Logger.getLogger(CargoTaxiCreator.class);

    public ArrayList<CargoTaxi> createCargoTaxi(String filename){
        ArrayList<CargoTaxi> cargoTaxiArrayList = new ArrayList<CargoTaxi>();
        CargoTaxi cargoTaxi;
        try {
            Scanner scan = new Scanner(new File(filename));
            while (scan.hasNextLine()){
                String str = scan.nextLine();
                String[] tokens = str.split(" ");
                cargoTaxi = new CargoTaxi(tokens[0], Integer.parseInt(tokens[1]),
                        Integer.parseInt(tokens[2]), Double.parseDouble(tokens[3]),
                        Double.parseDouble(tokens[4]), Boolean.parseBoolean(tokens[5]));
                /*cargoTaxi.setMakeOfTheCar(tokens[0]);
                cargoTaxi.setCost(Integer.parseInt(tokens[1]));
                cargoTaxi.setNumberOfSeats(Integer.parseInt(tokens[2]));
                cargoTaxi.setFuelConsumption(Double.parseDouble(tokens[3]));
                cargoTaxi.setLiftingCapasity(Double.parseDouble(tokens[4]));
                cargoTaxi.setTrailer(Boolean.parseBoolean(tokens[5]));*/
                cargoTaxiArrayList.add(cargoTaxi);
            }
        } catch (FileNotFoundException e) {
            logger.error("File is not found!", e);
        }
        return cargoTaxiArrayList;
    }
}
