package by.bsu.taxipark.entity;
/**
 * Created by user on 24.11.2015.
 */
public class Minibus extends Auto {
    private Integer maxNumberOfPassengers;

    public Integer getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    public void setMaxNumberOfPassengers(int maxNumberOfPassengers) {
        this.maxNumberOfPassengers = maxNumberOfPassengers;
    }

    public Minibus(){

    }

    public Minibus(String makeOfTheCar, Integer cost, Integer numberOfSeats, Double fuelConsumption, Double liftingCapacity, Integer maxNumberOfPassengers){
        super(makeOfTheCar, cost, numberOfSeats, fuelConsumption, liftingCapacity);
        setMaxNumberOfPassengers(maxNumberOfPassengers);
    }

    @Override
    public String toString() {
        return super.toString()+String.format("%7d", maxNumberOfPassengers);
    }
}
