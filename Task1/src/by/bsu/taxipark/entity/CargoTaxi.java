package by.bsu.taxipark.entity;
/**
 * Created by user on 24.11.2015.
 */
public class CargoTaxi extends Auto {
    private boolean trailer;

    public CargoTaxi(){

    }

    public boolean isTrailer() {
        return trailer;
    }

    public void setTrailer(boolean trailer) {
        this.trailer = trailer;
    }

    public CargoTaxi(String makeOfTheCar, Integer cost, Integer numberOfSeats, Double fuelConsumtion, Double liftingCapacity, Boolean trailer){
        super(makeOfTheCar, cost, numberOfSeats, fuelConsumtion, liftingCapacity);
        setTrailer(trailer);
    }

    @Override
    public String toString() {
        return super.toString()+String.format("%7b", trailer);
    }
}
