package by.bsu.taxipark.entity;
/**

 * Created by user on 24.11.2015.
 */
public abstract class Auto{
    private String makeOfTheCar;
    private Integer cost;
    private Integer numberOfSeats;
    private Double fuelConsumption;
    private Double liftingCapasity;

    public Auto(){

    }

    public Auto(String makeOfTheCar, Integer cost, Integer numberOfSeats, Double fuelConsumption, Double liftingCapasity){
        setMakeOfTheCar(makeOfTheCar);
        setCost(cost);
        setNumberOfSeats(numberOfSeats);
        setFuelConsumption(fuelConsumption);
        setLiftingCapasity(liftingCapasity);
    }

    public String getMakeOfTheCar() {
        return makeOfTheCar;
    }

    public Integer getCost() {
        return cost;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public Double getFuelConsumption() {
        return fuelConsumption;
    }

    public Double getLiftingCapasity() {
        return liftingCapasity;
    }

    public void setMakeOfTheCar(String makeOfTheCar) {
        this.makeOfTheCar = makeOfTheCar;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setFuelConsumption(Double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public void setLiftingCapasity(Double liftingCapasity) {
        this.liftingCapasity = liftingCapasity;
    }

    @Override
    public String toString() {
        return String.format("%25s%7d$%10d%8.2fл%8.2fт", makeOfTheCar, cost, numberOfSeats, fuelConsumption, liftingCapasity);
    }
}


