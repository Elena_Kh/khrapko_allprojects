package by.bsu.taxipark.entity;
/**
 * Created by user on 24.11.2015.
 */
public class Taxi extends Auto {
    private String vehicleClass;

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public Taxi(){

    }

    public Taxi(String makeOfTheCar, Integer cost, Integer numberOfSeats, Double fuelConsumption, Double liftingCapacity, String vehicleClass){
        super(makeOfTheCar, cost, numberOfSeats, fuelConsumption, liftingCapacity);
        setVehicleClass(vehicleClass);
    }

    @Override
    public String toString() {
        return super.toString()+String.format("%7s", vehicleClass);
    }
}
