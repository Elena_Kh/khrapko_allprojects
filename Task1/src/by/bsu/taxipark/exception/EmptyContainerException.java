package by.bsu.taxipark.exception;

/**
 * Created by user on 24.11.2015.
 */
public class EmptyContainerException extends Exception {

    public EmptyContainerException() {
    }

    public EmptyContainerException(String message) {
        super(message);
    }

    public EmptyContainerException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyContainerException(Throwable cause) {
        super(cause);
    }
}
