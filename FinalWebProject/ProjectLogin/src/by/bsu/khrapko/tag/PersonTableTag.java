package by.bsu.khrapko.tag;

import by.bsu.khrapko.domain.Person;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by user on 06.04.2016.
 */
public class PersonTableTag extends TagSupport {
    private String position;
    private String firstname;
    private String lastname;
    private String location;
    private String status;
    private String active, notactive;
    private List<String> head;

    private void initHead(){
        head = new ArrayList<>(5);
        head.add("");
        head.add(firstname);
        head.add(lastname);
        head.add(location);
        head.add(status);
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public void setNotactive(String notactive) {
        this.notactive = notactive;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int doStartTag() throws JspTagException {
        initHead();
        try {
            JspWriter out = pageContext.getOut();
            out.write("<table class=\"paginated table table-hover\" border=\"1\" style=\"width:100%\">");
            out.write("<thead><tr>");
            for (int i=0; i<head.size(); i++){
                out.write("<th scope='col'>" + head.get(i) + "</th>");
            }
            out.write("</tr></thead><tbody>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspTagException {
        ArrayList<Person> personList = (ArrayList<Person>) pageContext.getSession().getAttribute("personList");
        try {
            JspWriter out = pageContext.getOut();
            for (int i=0; i<personList.size(); i++){
                if (personList.get(i).getPosition().equals(position)){
                    Person p = personList.get(i);
                    try {
                        out.write("<tr><td><input type=\"radio\" name=\"idPerson\" value=\"" + p.getId() + "\"></td>");
                        out.write("<td>" + p.getFirstName() + "</td>");
                        out.write("<td>" + p.getLastName() + "</td>");
                        out.write("<td>" + p.getLocation() + "</td>");
                        if (p.getStatus()){
                            out.write("<td>" + active + "</td></tr>");
                        }else{
                            out.write("<td>" + notactive + "</td></tr>");
                        }
                    } catch (IOException e) {
                        throw new JspTagException(e.getMessage());
                    }
                }
            }
            pageContext.getOut().write("</tbody></table>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_PAGE;
    }
}
