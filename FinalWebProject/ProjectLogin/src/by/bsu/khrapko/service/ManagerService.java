package by.bsu.khrapko.service;

import by.bsu.khrapko.dao.AdminDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.ManagerDAO;
import by.bsu.khrapko.dao.TeamDAO;
import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.domain.Team;
import by.bsu.khrapko.domain.Trip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 26.02.2016.
 */
public class ManagerService {

    public List<String> showPositionList() throws ServiceException {
        try {
            return new ManagerDAO().selectAllPosition();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean insertPerson(Person p) throws ServiceException {
        try {
            return new ManagerDAO().create(p);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updatePerson(Person p) throws ServiceException {
        try {
            return new ManagerDAO().update(p);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Person selectPerson(Long id) throws ServiceException {
        try {
            return new ManagerDAO().select(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean deletePerson(Long id) throws ServiceException {
        try {
            return new ManagerDAO().delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Person> showPersonList() throws ServiceException {
        try {
            return new ManagerDAO().selectAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Person> showPersonList(Long id) throws ServiceException {
        List<Person> resultList = new ArrayList<>();
        try {
            List<Person> personList = new ManagerDAO().selectAll(id);
            for (Person person:personList){
                List<Trip> tripForPerson = new AdminDAO().selectTripByPerson(person.getId());
                if (checkDateForPerson(id, tripForPerson)){
                    resultList.add(person);
                }
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return resultList;
    }

    private boolean checkDateForPerson(Long tripId, List<Trip> tripForPerson) throws DAOException {
        Trip trip = new AdminDAO().select(tripId);
        int index = tripForPerson.indexOf(trip);
        if (index >= 0) {
            tripForPerson.remove(index);
        }
        if (checkInside(trip, tripForPerson)){
            return checkOutside(trip, tripForPerson);
        }else return false;
    }

    private boolean checkInside(Trip trip, List<Trip> tripList){
        for (Trip tmpTrip: tripList){
            if ((trip.getDateDeparture().compareTo(tmpTrip.dateArrival(tmpTrip.getDateArrival(), tmpTrip.getDuration())) < 0
                    && trip.getDateDeparture().compareTo(tmpTrip.getDateDeparture()) > 0)
                    ||  (trip.dateArrival(trip.getDateArrival(), trip.getDuration()).compareTo(tmpTrip.getDateDeparture()) > 0
                    && trip.dateArrival(trip.getDateArrival(), trip.getDuration()).compareTo(
                    tmpTrip.dateArrival(tmpTrip.getDateArrival(), tmpTrip.getDuration())) < 0)){
                return false;
            }
        }
        return true;
    }

    private boolean checkOutside(Trip trip, List<Trip> tripList){
        for (Trip tmpTrip : tripList){
            if (trip.dateArrival(trip.getDateArrival(), trip.getDuration()).compareTo(
                    tmpTrip.dateArrival(tmpTrip.getDateArrival(), tmpTrip.getDuration())) > 0 &&
                    trip.getDateDeparture().compareTo(tmpTrip.getDateDeparture()) < 0){
                return false;
            }
        }
        return true;
    }

    public HashMap<Long, Team> showTeamList() throws ServiceException {
        try {
            return new TeamDAO().selectAllTeam();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Team selectTeam(Long id) throws ServiceException {
        try {
            return new TeamDAO().select(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updateTeam(Long id, String... persons) throws ServiceException{
        Team team = new Team();
        for (int i=0; i<persons.length; i++){
            if (persons[i] != null){
                team.setTeam(persons[i], i+1);
            }
        }
        try {
            return new TeamDAO().update(id, team);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<String> showCountryList() throws ServiceException {
        try {
            return new ManagerDAO().selectAllCountry();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean allowPersonEdit(String id) throws ServiceException {
        try {
            return new ManagerDAO().allowEdit(Long.parseLong(id));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
