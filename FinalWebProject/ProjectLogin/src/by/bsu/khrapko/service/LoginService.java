package by.bsu.khrapko.service;

import by.bsu.khrapko.dao.LoginDAO;
import by.bsu.khrapko.dao.RegistrationDAO;
import by.bsu.khrapko.domain.User;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.md5.MD5Digest;

/**
 * Created by user on 31.01.2016.
 */
public class LoginService {
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean checkLogin(String enterLogin, String enterPass) throws ServiceException {
        LoginDAO dao = new LoginDAO();
        try {
            User user = dao.select(enterLogin);
            role = new String(user.getRole());
            return new MD5Digest().md5(enterPass).equals(user.getPassword());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean checkUser(String enterLogin) throws ServiceException {
        RegistrationDAO dao = new RegistrationDAO();
        try {
            User user = dao.select(enterLogin);
            if (user == null){
                return true;
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean insertUser(String login, String password, String role) throws ServiceException {
        User user = new User(login, password);
        if (role.equals("admin")){
            user.setRole(new String("1"));
        }else{
            user.setRole(new String("2"));
        }
        RegistrationDAO dao = new RegistrationDAO();
        try {
            return dao.create(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
