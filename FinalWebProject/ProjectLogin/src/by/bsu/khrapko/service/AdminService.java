package by.bsu.khrapko.service;

import by.bsu.khrapko.dao.AdminDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.RegistrationDAO;
import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.domain.User;

import java.util.List;

/**
 * Created by user on 14.02.2016.
 */
public class AdminService {

    public List<Trip> showTripList() throws ServiceException {
        try {
            return new AdminDAO().selectAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean insertTrip(Trip trip) throws ServiceException {
        try {
            return new AdminDAO().create(trip);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean updateTrip(Trip trip) throws ServiceException {
        try {
            return new AdminDAO().update(trip);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<String> showCountryList() throws ServiceException {
        try {
            return new AdminDAO().selectAllCountry();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Trip selectTrip(Long id) throws ServiceException {
        try {
            return new AdminDAO().select(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean deleteTrip(Long id) throws ServiceException{
        try {
            return new AdminDAO().delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean allowEdit(String id) throws ServiceException {
        try {
            return new AdminDAO().allowEdit(Long.parseLong(id));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
