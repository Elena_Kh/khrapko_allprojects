package by.bsu.khrapko.domain;

/**
 * Created by user on 28.02.2016.
 */
public class Team extends Entity{
    private String pilot;
    private String navigator;
    private String radioOperator;
    private String stewardess;

    public Team() {
    }

    public Team(String pilot, String navigator, String radioOperator, String stewardess) {
        this.pilot = pilot;
        this.navigator = navigator;
        this.radioOperator = radioOperator;
        this.stewardess = stewardess;
    }

    public void setTeam(String person, int position){
        switch (position){
            case 1:{
                setPilot(person);
                break;
            }
            case 2:{
                setNavigator(person);
                break;
            }
            case 3:{
                setRadioOperator(person);
                break;
            }
            case 4:{
                setStewardess(person);
                break;
            }
        }
    }

    public String getTeam(int position){
        switch (position){
            case 1:{
                return getPilot();
            }
            case 2:{
                return getNavigator();
            }
            case 3:{
                return getRadioOperator();
            }
            case 4:{
                return getStewardess();
            }
            default: return null;
        }
    }

    public void setPilot(String pilot) {
        this.pilot = pilot;
    }

    public void setNavigator(String navigator) {
        this.navigator = navigator;
    }

    public void setRadioOperator(String radioOperator) {
        this.radioOperator = radioOperator;
    }

    public void setStewardess(String stewardess) {
        this.stewardess = stewardess;
    }

    public String getPilot() {
        return pilot;
    }

    public String getNavigator() {
        return navigator;
    }

    public String getRadioOperator() {
        return radioOperator;
    }

    public String getStewardess() {
        return stewardess;
    }
}
