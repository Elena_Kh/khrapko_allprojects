package by.bsu.khrapko.domain;

/**
 * Created by user on 26.02.2016.
 */
public class Person extends Entity {
    private Long id;
    private String firstName;
    private String lastName;
    private String position;
    private String location;
    private Boolean status;

    public Person(Long id, String firstName, String lastName, String position) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
    }

    public Person(String firstName, String lastName, String position, String location, String status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.location = location;
        if("active".equals(status)) {
            this.status = true;
        }else{
            this.status = false;
        }
    }

    public Person(String firstName, String lastName, String position, String location, int status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.location = location;
        if(status == 1) {
            this.status = true;
        }else{
            this.status = false;
        }
    }

    public Person(Long id, String firstName, String lastName, String position, String location, Boolean status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.location = location;
        this.status = status;
    }

    public Person(Long id, String firstName, String lastName, String position, String location, String status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.location = location;
        if("active".equals(status)) {
            this.status = true;
        }else{
            this.status = false;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
