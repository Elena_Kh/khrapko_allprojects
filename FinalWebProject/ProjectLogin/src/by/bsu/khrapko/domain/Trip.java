package by.bsu.khrapko.domain;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by user on 14.02.2016.
 */
public class Trip extends Entity {
    private Long id;
    private String countryDeparture;
    private String countryArrival;
    private GregorianCalendar dateDeparture;
    private GregorianCalendar dateArrival;
    private GregorianCalendar duration;
    private Double tzDeparture;
    private Double tzArrival;

    public Trip() {
    }

    public Trip(Long id, String countryDeparture, String countryArrival, String dateDeparture,
                String dateArrival, String duration, Double tzDeparture, Double tzArrival) {
        this.id = id;
        this.countryDeparture = countryDeparture;
        this.countryArrival = countryArrival;
        setDateDeparture(dateDeparture);
        setDateArrival(dateArrival);
        setDuration(duration);
        this.tzDeparture = tzDeparture;
        this.tzArrival = tzArrival;
    }

    /*public Trip(String countryDeparture, String countryArrival, String dateDeparture, String dateArrival,
                String duration, Double tzDeparture, Double tzArrival) {
        this.countryDeparture = countryDeparture;
        this.countryArrival = countryArrival;
        setDateDeparture(dateDeparture);
        setDateArrival(dateArrival);
        setDuration(duration);
        this.tzDeparture = tzDeparture;
        this.tzArrival = tzArrival;
    }

    public Trip(String dateDeparture, String dateArrival, String duration, Double tzDeparture, Double tzArrival) {
        setDateDeparture(dateDeparture);
        setDateArrival(dateArrival);
        setDuration(duration);
        this.tzDeparture = tzDeparture;
        this.tzArrival = tzArrival;
    }*/

    public Trip(String countryDeparture, String countryArrival, String dateDeparture, String dateArrival, String duration) {
        this.countryDeparture = countryDeparture;
        this.countryArrival = countryArrival;
        setDateDeparture(dateDeparture);
        setDateArrival(dateArrival);
        setDuration(duration);
    }

    public String dateToString(GregorianCalendar gc){
        return String.format("%1$tY-%1$tm-%1$td", gc);
    }

    public String timeToString(GregorianCalendar gc){
        return String.format("%1$tH:%1$tM", gc);
    }

    public GregorianCalendar dateArrival(GregorianCalendar date, GregorianCalendar duration){
        GregorianCalendar gr = new GregorianCalendar(date.get(GregorianCalendar.YEAR), date.get(GregorianCalendar.MONTH),
                date.get(GregorianCalendar.DAY_OF_MONTH), date.get(GregorianCalendar.HOUR_OF_DAY), date.get(GregorianCalendar.MINUTE));
        gr.add(GregorianCalendar.MINUTE, duration.get(GregorianCalendar.MINUTE));
        gr.add(GregorianCalendar.HOUR_OF_DAY, duration.get(GregorianCalendar.HOUR_OF_DAY));
        gr.add(GregorianCalendar.HOUR_OF_DAY,(int) (tzArrival - tzDeparture));
        return gr;
    }

    public String getCountryDeparture() {
        return countryDeparture;
    }

    public void setCountryDeparture(String countryDeparture) {
        this.countryDeparture = countryDeparture;
    }

    public String getCountryArrival() {
        return countryArrival;
    }

    public void setCountryArrival(String countryArrival) {
        this.countryArrival = countryArrival;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GregorianCalendar getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(GregorianCalendar dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public void setDateDeparture(String str){
        dateDeparture = new GregorianCalendar();
        String[] lexem = str.split("[-:\\p{Blank}]");
        dateDeparture.set(Integer.parseInt(lexem[0]), Integer.parseInt(lexem[1])-1, Integer.parseInt(lexem[2]),
                Integer.parseInt(lexem[3]), Integer.parseInt(lexem[4]));
    }

    public GregorianCalendar getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(GregorianCalendar dateArrival) {
        this.dateArrival = dateArrival;
    }

    public void setDateArrival(String str){
        dateArrival = new GregorianCalendar();
        String[] lexem = str.split("[-:\\p{Blank}]");
        dateArrival.set(Integer.parseInt(lexem[0]), Integer.parseInt(lexem[1])-1, Integer.parseInt(lexem[2]),
                Integer.parseInt(lexem[3]), Integer.parseInt(lexem[4]));
    }

    public GregorianCalendar getDuration() {
        return duration;
    }

    public void setDuration(GregorianCalendar duration) {
        this.duration = duration;
    }

    public void setDuration(String str){
        duration = new GregorianCalendar();
        String[] lexem = str.split("[:]");
        duration.set(2016, 01, 01, Integer.parseInt(lexem[0]), Integer.parseInt(lexem[1]));
    }

    public String departureToString(){
        return new String(countryDeparture + "   " + dateToString(dateDeparture) + " " + timeToString(dateDeparture) + "   " +
                timeToString(duration) + "   " + dateToString(dateArrival(dateDeparture, duration)) + " " +
                timeToString(dateArrival(dateDeparture, duration)) + "   " + countryArrival);
    }

    public String arrivalToString(){
        return new String(countryArrival + "   " + dateToString(dateArrival) + " " + timeToString(dateArrival) + "   " +
                timeToString(duration) + "   " + dateToString(dateArrival(dateArrival, duration)) + " " +
                timeToString(dateArrival(dateArrival, duration)) + "   " + countryDeparture);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trip)) return false;

        Trip trip = (Trip) o;

        return getId() != null ? getId().equals(trip.getId()) : trip.getId() == null;

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
