package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 03.03.2016.
 */
public class PreEditPersonCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(PreEditPersonCommand.class);

    private static final String PARAM_NAME_ID = "idPerson";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.managerEditPerson");
        String id = request.getParameter(PARAM_NAME_ID);
        ManagerService service = new ManagerService();
        try {
            if (id != null){
                if (service.allowPersonEdit(id)) {
                    Person person = service.selectPerson(Long.parseLong(id));
                    person.setId(Long.parseLong(id));
                    request.getSession().setAttribute("person", person);
                }else{
                    request.setAttribute("errorSelectMessage",
                            MessageManager.chooseLocale(request.getParameter("locale")).getProperty("message.person.selecterror"));
                    page = ConfigurationManager.getProperty("path.page.managerPersonList");
                }
            }else {
                request.setAttribute("errorSelectMessage",
                        MessageManager.chooseLocale(request.getParameter("locale")).getProperty("message.selectpersonerror"));
                page = ConfigurationManager.getProperty("path.page.managerPersonList");
            }
        } catch (ServiceException e) {
            logger.error(e);
        }
        return page;
    }
}
