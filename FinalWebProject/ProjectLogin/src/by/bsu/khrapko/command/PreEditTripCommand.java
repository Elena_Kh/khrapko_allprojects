package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 25.02.2016.
 */
public class PreEditTripCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(PreEditTripCommand.class);

    private static final String PARAM_NAME_ID = "idTrip";
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.adminEdit");
        String id = request.getParameter(PARAM_NAME_ID);
        AdminService service = new AdminService();
        try {
            if (id != null){
                if (service.allowEdit(id)) {
                    Trip trip = service.selectTrip(Long.parseLong(id));
                    request.getSession().setAttribute("idTrip", id);
                    request.getSession().setAttribute("countryDeparture", trip.getCountryDeparture());
                    request.getSession().setAttribute("countryArrival", trip.getCountryArrival());
                    request.getSession().setAttribute("duration", trip.timeToString(trip.getDuration()));
                    request.getSession().setAttribute("dateDeparture", trip.dateToString(trip.getDateDeparture()));
                    request.getSession().setAttribute("timeDeparture", trip.timeToString(trip.getDateDeparture()));
                    request.getSession().setAttribute("dateArrival", trip.dateToString(trip.getDateArrival()));
                    request.getSession().setAttribute("timeArrival", trip.timeToString(trip.getDateArrival()));
                }else{
                    request.setAttribute("errorSelectMessage",
                            MessageManager.chooseLocale((String) request.getSession().getAttribute("locale")).getProperty("message.trip.selecterror"));
                    page = ConfigurationManager.getProperty("path.page.adminList");
                }
            }else {
                String str = (String) request.getSession().getAttribute("locale");
                request.setAttribute("errorSelectMessage",
                        MessageManager.chooseLocale(str).getProperty("message.selectediterror"));
                page = ConfigurationManager.getProperty("path.page.adminList");
            }
        } catch (ServiceException e) {
                logger.error(e);
        }
        return page;
    }
}
