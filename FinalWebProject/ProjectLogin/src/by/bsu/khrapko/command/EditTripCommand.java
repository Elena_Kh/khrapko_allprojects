package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 22.02.2016.
 */
public class EditTripCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(EditTripCommand.class);

    private static final String PARAM_NAME_ID_TRIP = "idTrip";
    private static final String PARAM_NAME_COUNTRY_DEPARTURE = "countryDeparture";
    private static final String PARAM_NAME_DATE_DEPARTURE = "dateDeparture";
    private static final String PARAM_NAME_TIME_DEPARTURE = "timeDeparture";
    private static final String PARAM_NAME_DURATION = "duration";
    private static final String PARAM_NAME_DATE_ARRIVAL = "dateArrival";
    private static final String PARAM_NAME_TIME_ARRIVAL = "timeArrival";
    private static final String PARAM_NAME_COUNTRY_ARRIVAL = "countryArrival";


    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        Long id = Long.parseLong(request.getParameter(PARAM_NAME_ID_TRIP));
        String countryDeparture = request.getParameter(PARAM_NAME_COUNTRY_DEPARTURE);
        String dateDeparture = request.getParameter(PARAM_NAME_DATE_DEPARTURE);
        String timeDeparture = request.getParameter(PARAM_NAME_TIME_DEPARTURE);
        String duration = request.getParameter(PARAM_NAME_DURATION);
        String dateArrival = request.getParameter(PARAM_NAME_DATE_ARRIVAL);
        String timeArrival = request.getParameter(PARAM_NAME_TIME_ARRIVAL);
        String countryArrival = request.getParameter(PARAM_NAME_COUNTRY_ARRIVAL);
        AdminService service = new AdminService();
        if (Validator.validateTrip(countryDeparture, dateDeparture + " " + timeDeparture, duration,
                dateArrival + " " + timeArrival, countryArrival)){
            Trip trip = new Trip(countryDeparture, countryArrival, dateDeparture + " " + timeDeparture,
                    dateArrival + " " + timeArrival, duration);
            trip.setId(id);
            try {
                service.updateTrip(trip);
                request.getSession().setAttribute("tripList", service.showTripList());
            } catch (ServiceException e) {
                logger.error(e);
            }
            page = ConfigurationManager.getProperty("path.page.adminList");
        }else{
            request.setAttribute("errorValidateMessage", Validator.errorValidate);
            page = ConfigurationManager.getProperty("path.page.adminEdit");
        }
        return page;
    }
}
