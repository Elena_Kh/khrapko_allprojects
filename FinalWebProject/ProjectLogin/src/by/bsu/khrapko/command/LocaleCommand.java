package by.bsu.khrapko.command;

import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 26.03.2016.
 */
public class LocaleCommand implements ActionCommand {

    static Logger logger = LogManager.getLogger(LocaleCommand.class);

    private static final String PARAM_LOCALE = "loc";
    private static final String PARAM_PAGE = "pageName";

    @Override
    public String execute(HttpServletRequest request) {
        String pageName = (String) request.getSession().getAttribute(PARAM_PAGE);
        String page = ConfigurationManager.getProperty("path.page." + pageName);
        if ("ru".equals(request.getParameter(PARAM_LOCALE))){
            request.getSession().setAttribute("locale", "ru_RU");
        }else if ("en".equals(request.getParameter(PARAM_LOCALE))){
            request.getSession().setAttribute("locale", "en_US");
        }else{
            request.getSession().setAttribute("locale", "de_DE");
        }
        return page;
    }
}
