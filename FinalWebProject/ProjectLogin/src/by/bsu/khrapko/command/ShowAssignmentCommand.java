package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Team;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 28.02.2016.
 */
public class ShowAssignmentCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.managerTripTeam");
        ManagerService managerService = new ManagerService();
        AdminService adminService = new AdminService();
        try {
            request.getSession().setAttribute("teamList", managerService.showTeamList());
            request.getSession().setAttribute("tripList", adminService.showTripList());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return page;
    }
}
