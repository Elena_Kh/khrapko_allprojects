package by.bsu.khrapko.command;
import javax.servlet.http.HttpServletRequest;
/**
 * Created by user on 31.01.2016.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}
