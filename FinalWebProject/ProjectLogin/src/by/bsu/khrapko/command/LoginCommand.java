package by.bsu.khrapko.command;
import javax.servlet.http.HttpServletRequest;

import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.service.LoginService;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by user on 31.01.2016.
 */
public class LoginCommand implements ActionCommand{
    static Logger logger = LogManager.getLogger(LoginCommand.class);

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private MessageManager messageManager;

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        try {
            LoginService service = new LoginService();
            if (Validator.validateLogin(login) && Validator.validatePassword(pass) && service.checkLogin(login, pass)) {
                request.getSession().setAttribute("user", service.getRole());
                request.getSession().setAttribute("userName", login);
                if (service.getRole().equals("admin")){
                    AdminService adminService = new AdminService();
                    List<String> countryList = adminService.showCountryList();
                    request.getSession().setAttribute("arrivalList", countryList);
                    request.getSession().setAttribute("departureList", countryList.subList(11, 14));
                }else{
                    ManagerService managerService = new ManagerService();
                    request.getSession().setAttribute("positionList", managerService.showPositionList());
                    request.getSession().setAttribute("departureList", managerService.showCountryList().subList(11,14));
                }
                page = ConfigurationManager.getProperty("path.page." + service.getRole());
            } else {
                String str = (String) request.getSession().getAttribute("locale");
                request.setAttribute("errorLoginPassMessage",
                        (MessageManager.chooseLocale(str)).getProperty("message.loginerror"));
                page = ConfigurationManager.getProperty("path.page.login");
            }
        } catch (ServiceException e) {
            logger.error(e);
        }
        return page;
    }
}
