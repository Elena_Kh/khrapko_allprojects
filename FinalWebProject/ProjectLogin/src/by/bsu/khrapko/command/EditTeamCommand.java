package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by user on 10.03.2016.
 */
public class EditTeamCommand implements ActionCommand {

    static Logger logger = LogManager.getLogger(EditTeamCommand.class);

    private static final String PARAM_NAME_ID = "idTrip";
    private static final String PARAM_NAME_PILOT = "pilot";
    private static final String PARAM_NAME_NAVIGATOR = "navigator";
    private static final String PARAM_NAME_RADIO_OPERATOR = "radioOperator";
    private static final String PARAM_NAME_STEWARDESS = "stewardess";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.managerTripTeam");
        String id = (String) request.getSession().getAttribute(PARAM_NAME_ID);//getParameter(PARAM_NAME_ID);
        String pilot = request.getParameter(PARAM_NAME_PILOT);
        String navigator = request.getParameter(PARAM_NAME_NAVIGATOR);
        String radioOperator = request.getParameter(PARAM_NAME_RADIO_OPERATOR);
        String stewardess = request.getParameter(PARAM_NAME_STEWARDESS);
        if (id != null){
            ManagerService managerService = new ManagerService();
            try {
                managerService.updateTeam(Long.parseLong(id), pilot, navigator, radioOperator, stewardess);
                request.getSession().setAttribute("teamList", managerService.showTeamList());
            } catch (ServiceException e) {
                logger.error(e);
            }
        }else {
            String str = (String) request.getSession().getAttribute("locale");
            request.setAttribute("errorSelectMessage",
                    MessageManager.chooseLocale(str).getProperty("message.selectediterror"));
            page = ConfigurationManager.getProperty("path.page.managerEditTeam");
        }
        return page;
    }
}
