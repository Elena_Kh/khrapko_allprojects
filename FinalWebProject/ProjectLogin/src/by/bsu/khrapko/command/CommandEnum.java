package by.bsu.khrapko.command;

/**
 * Created by user on 31.01.2016.
 */
public enum CommandEnum {
    LOGIN {{this.command = new LoginCommand();}},
    REGISTRATION{{this.command = new RegistrationCommand();}},
    LOGOUT {{this.command = new LogoutCommand();}},
    SHOWLISTTRIP{{this.command = new ShowTripListCommand();}},
    ADDTRIP{{this.command = new AddTripCommand();}},
    EDIT{{this.command = new EditTripCommand();}},
    EDITTRIP{{this.command = new PreEditTripCommand();}},
    DELETETRIP{{this.command = new DeleteTripCommand();}},
    ADDPERSON{{this.command = new AddPersonCommand();}},
    EDITPERSON{{this.command = new PreEditPersonCommand();}},
    SAVEPERSON{{this.command = new EditPersonCommand();}},
    DELETEPERSON{{this.command = new DeletePersonCommand();}},
    SHOWPERSONLIST{{this.command = new ShowPersonListCommand();}},
    SHOWTRIPLISTWITHTEAM{{this.command = new ShowAssignmentCommand();}},
    EDITTEAM{{this.command = new PreEditTeamCommand();}},
    SAVETEAM{{this.command = new EditTeamCommand();}},
    LOCALE{{this.command = new LocaleCommand();}};
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
