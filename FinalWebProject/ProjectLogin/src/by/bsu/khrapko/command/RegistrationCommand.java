package by.bsu.khrapko.command;

import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.LoginService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by user on 20.02.2016.
 */
public class RegistrationCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(RegistrationCommand.class);

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_POSITION = "position";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String position = request.getParameter(PARAM_NAME_POSITION);
        try {
            LoginService service = new LoginService();
            if (Validator.validateLogin(login) && Validator.validatePassword(pass) && service.checkUser(login)) {
                if (service.insertUser(login, pass, position)) {
                    request.getSession().setAttribute("user", position);
                    request.getSession().setAttribute("userName", login);
                    if (position.equals("admin")){
                        AdminService adminService = new AdminService();
                        List<String> countryList = adminService.showCountryList();
                        request.getSession().setAttribute("arrivalList", countryList);
                        request.getSession().setAttribute("departureList", countryList.subList(10,13));
                    }else{
                        ManagerService managerService = new ManagerService();
                        request.getSession().setAttribute("positionList", managerService.showPositionList());
                        request.getSession().setAttribute("departureList", managerService.showCountryList().subList(10,13));
                    }
                    page = ConfigurationManager.getProperty("path.page." + position);
                }
            } else {
                request.setAttribute("errorRegistrMessage",
                         MessageManager.chooseLocale((String)request.getSession().getAttribute("locale")).getProperty("message.loginexistence"));
                page = ConfigurationManager.getProperty("path.page.registration");
            }
        } catch (ServiceException e) {
            logger.error(e);
        }
        return page;
    }
}
