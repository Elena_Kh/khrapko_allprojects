package by.bsu.khrapko.command;

import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 04.03.2016.
 */
public class DeletePersonCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(DeletePersonCommand.class);

    private static final String PARAM_NAME_ID_PERSON = "idPerson";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String id = request.getParameter(PARAM_NAME_ID_PERSON);
        if (id != null){
            ManagerService service = new ManagerService();
            try {
                service.deletePerson(Long.parseLong(id));
                request.getSession().setAttribute("personList", service.showPersonList());
            } catch (ServiceException e) {
                logger.error(e);
            }
        }else{
            String str = (String) request.getSession().getAttribute("locale");
            request.setAttribute("errorSelectMessage",
                    MessageManager.chooseLocale(str).getProperty("message.person.selectdeleteerror"));
        }
        page = ConfigurationManager.getProperty("path.page.managerPersonList");
        return page;
    }
}
