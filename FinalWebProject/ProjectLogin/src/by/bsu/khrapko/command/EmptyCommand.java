package by.bsu.khrapko.command;

import javax.servlet.http.HttpServletRequest;
import by.bsu.khrapko.util.ConfigurationManager;
/**
 * Created by user on 31.01.2016.
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.login");
        return page;
    }
}
