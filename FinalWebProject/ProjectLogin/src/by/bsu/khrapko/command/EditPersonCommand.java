package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 03.03.2016.
 */
public class EditPersonCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(EditPersonCommand.class);

    private static final String PARAM_NAME_ID = "idPerson";
    private static final String PARAM_NAME_FIRST_NAME = "firstname";
    private static final String PARAM_NAME_LAST_NAME = "lastname";
    private static final String PARAM_NAME_POSITION = "position";
    private static final String PARAM_NAME_LOCATION = "location";
    private static final String PARAM_NAME_STATUS = "status";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String id = request.getParameter(PARAM_NAME_ID);
        String firstname = request.getParameter(PARAM_NAME_FIRST_NAME);
        String lastname = request.getParameter(PARAM_NAME_LAST_NAME);
        String position = request.getParameter(PARAM_NAME_POSITION);
        String location = request.getParameter(PARAM_NAME_LOCATION);
        String status = request.getParameter(PARAM_NAME_STATUS);
        ManagerService service = new ManagerService();
        if (Validator.validatePerson(firstname, lastname)){
            Person p = new Person(Long.parseLong(id), firstname, lastname, position, location, status);
            try {
                service.updatePerson(p);
                request.getSession().setAttribute("personList", service.showPersonList());
            } catch (ServiceException e) {
                logger.error(e);
            }
            page = ConfigurationManager.getProperty("path.page.managerPersonList");
        }else{
            request.setAttribute("errorValidateMessage", Validator.errorValidate);
            page = ConfigurationManager.getProperty("path.page.manager");
        }
        return page;
    }
}
