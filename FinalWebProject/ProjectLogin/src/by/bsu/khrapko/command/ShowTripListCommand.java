package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by user on 14.02.2016.
 */
public class ShowTripListCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(ShowTripListCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.adminList");;
        AdminService service = new AdminService();
        try {
            List<Trip> tripList = service.showTripList();
            request.getSession().setAttribute("tripList", service.showTripList());
        } catch (ServiceException e) {
            logger.error(e);
        }
        return page;
    }
}
