package by.bsu.khrapko.command;

import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 29.02.2016.
 */
public class DeleteTripCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(DeleteTripCommand.class);

    private static final String PARAM_NAME_ID_TRIP = "idTrip";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String id = request.getParameter(PARAM_NAME_ID_TRIP);
        if (id != null){
            AdminService service = new AdminService();
            try {
                service.deleteTrip(Long.parseLong(id));
                request.getSession().setAttribute("tripList", service.showTripList());
            } catch (ServiceException e) {
                logger.error(e);
            }
        }else{
            String str = (String) request.getSession().getAttribute("locale");
            request.setAttribute("errorSelectMessage",
                    MessageManager.chooseLocale(str).getProperty("message.selectdeleteerror"));
        }
        page = ConfigurationManager.getProperty("path.page.adminList");
        return page;
    }
}
