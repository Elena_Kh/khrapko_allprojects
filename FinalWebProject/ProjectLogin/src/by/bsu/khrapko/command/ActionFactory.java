package by.bsu.khrapko.command;
import javax.servlet.http.HttpServletRequest;

import by.bsu.khrapko.util.MessageManager;
/**
 * Created by user on 31.01.2016.
 */
public class ActionFactory {
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action
                    + MessageManager.chooseLocale(request.getParameter("locale")).getProperty("message.wrongaction"));
        }
        return current;
    }
}
