package by.bsu.khrapko.command;

import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import by.bsu.khrapko.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by user on 28.02.2016.
 */
public class PreEditTeamCommand implements ActionCommand {

    static Logger logger = LogManager.getLogger(PreEditTeamCommand.class);

    private static final String PARAM_NAME_ID = "idTrip";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.managerEditTeam");
        String id = request.getParameter(PARAM_NAME_ID);
        if (id != null){
            AdminService adminService = new AdminService();
            ManagerService managerService = new ManagerService();
            try {
                Trip trip = adminService.selectTrip(Long.parseLong(id));
                List<Person> personList = managerService.showPersonList(Long.parseLong(id));
                request.getSession().setAttribute("idTrip", id);//////getSession
                request.getSession().setAttribute("tripDeparture", trip.departureToString());
                request.getSession().setAttribute("tripArrival", trip.arrivalToString());
                request.getSession().setAttribute("personList", personList);
                request.getSession().setAttribute("team", managerService.selectTeam(Long.parseLong(id)));
            } catch (ServiceException e) {
                logger.error(e);
            }
        }else {
            String str = (String) request.getSession().getAttribute("locale");
            request.setAttribute("errorSelectMessage",
                    MessageManager.chooseLocale(str).getProperty("message.selectediterror"));
            page = ConfigurationManager.getProperty("path.page.managerTripTeam");
        }
        return page;
    }
}
