package by.bsu.khrapko.command;

import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 26.02.2016.
 */
public class ShowPersonListCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(ShowPersonListCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.managerPersonList");
        ManagerService service = new ManagerService();
        try {
            request.getSession().setAttribute("personList", service.showPersonList());
        } catch (ServiceException e) {
            logger.error(e);
        }
        return page;
    }
}
