package by.bsu.khrapko.validator;

import by.bsu.khrapko.domain.Trip;

import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * Created by user on 03.02.2016.
 */
public class Validator {
    private final static String LOGIN_REGEX = "\\w{5,20}";
    private final static String NAME_REGEX = "[A-ZА-Я][a-zа-я]{1,10}";
    public static String errorValidate;

    public static boolean validateLogin(String login){
        if (login == null){
            return false;
        }else{
            return Pattern.matches(LOGIN_REGEX, login);
        }
    }

    public static boolean validatePassword(String pass){
        if (pass == null){
            return false;
        }else{
            return Pattern.matches(LOGIN_REGEX, pass);
        }
    }

    public static boolean validateTrip(String countryDeparture, String dateDeparture,
                                       String duration, String dateArrival, String countryArrival){
        if (countryDeparture.equals(countryArrival)){
            errorValidate = "message.trip.countries";
            return false;
        }else if ((duration == null) || (duration.isEmpty()) || ("00:00".equals(duration))) {
            errorValidate = "message.trip.duration";
            return false;
        } else {
            Trip trip = new Trip();
            trip.setDateDeparture(dateDeparture);
            trip.setDateArrival(dateArrival);
            if (Calendar.getInstance().compareTo(trip.getDateDeparture()) > 0){
                errorValidate = "message.trip.departureDate";
                return false;
            }
            if (Calendar.getInstance().compareTo(trip.getDateDeparture()) > 0){
                errorValidate = "message.trip.arrivalDate";
                return false;
            }
            if (trip.getDateDeparture().compareTo(trip.getDateArrival()) > 0){
                errorValidate = "message.trip.date";
                return false;
            }
        }
        return true;
    }

    public static boolean validatePerson(String firstname, String lastname){
        return (Pattern.matches(NAME_REGEX, firstname) && Pattern.matches(NAME_REGEX, lastname));
    }
}
