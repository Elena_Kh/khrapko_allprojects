package by.bsu.khrapko.util;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Created by user on 31.01.2016.
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config", Locale.getDefault());
    private ConfigurationManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
