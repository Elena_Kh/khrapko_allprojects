package by.bsu.khrapko.util;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Created by user on 31.01.2016.
 */

public enum MessageManager {
    RU(ResourceBundle.getBundle("resources.messages", new Locale("ru", "RU"))),
    EN(ResourceBundle.getBundle("resources.messages", new Locale("en", "US"))),
    DE(ResourceBundle.getBundle("resources.messages", new Locale("de", "DE")));

    private ResourceBundle resourceBundle;

    public static MessageManager chooseLocale(String str){
        if("en_US".equals(str)){
            return MessageManager.EN;
        }else if ("de_DE".equals(str)){
            return MessageManager.DE;
        }else{
            return MessageManager.RU;
        }
    }
    MessageManager(ResourceBundle bundle){
        this.resourceBundle = bundle;
    }

    public String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
