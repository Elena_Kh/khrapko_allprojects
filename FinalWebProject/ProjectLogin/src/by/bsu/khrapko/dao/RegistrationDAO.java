package by.bsu.khrapko.dao;

import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.domain.User;
import by.bsu.khrapko.md5.MD5Digest;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 20.02.2016.
 */
public class RegistrationDAO extends AbstractDAO<String,User> {
    static Logger logger = LogManager.getLogger(RegistrationDAO.class);

    public static final String SQL_SELECT_USER_BY_LOGIN = "SELECT * FROM user WHERE login=?";
    public static final String SQL_INSERT_USER = "INSERT INTO user (login, password, role) VALUES (?, ?, ?);";

    @Override
    public List<User> selectAll() throws DAOException {
        return null;
    }

    @Override
    public User select(String login) throws DAOException {
        User user = new User();
        if (Validator.validateLogin(login)) {
            ProxyConnection cn = null;
            PreparedStatement st = null;
            try {
                cn = ConnectionPool.getInstance().getConnection();
                st = cn.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
                st.setString(1, login);
                ResultSet resultSet = st.executeQuery();
                if (resultSet.next()){
                    return user;
                }else{
                    return null;
                }
            } catch (SQLException e) {
                throw new DAOException("SQL exception (request or table failed): ", e);
            } finally {
                close(st);
                close(cn);
            }
        }
        return null;
    }

    @Override
    public boolean delete(String id) {
        return false;
    }

    @Override
    public boolean create(User user) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_INSERT_USER);
            st.setString(1, user.getLogin());
            st.setString(2, new MD5Digest().md5(user.getPassword()));
            st.setString(3, user.getRole());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
            return false;
        }

    }

    @Override
    public boolean update(User entity) {
        return false;
    }
}
