package by.bsu.khrapko.dao;

import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.domain.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 02.02.2016.
 */
public abstract class AbstractDAO<K, T extends Entity> {

    static Logger logger = LogManager.getLogger(AbstractDAO.class);

    private static final String SQL_SELECT_COUNTRY = "SELECT idcountry FROM country WHERE country_name = ?";
    private static final String SQL_SELECT_ALL_COUNTRY = "SELECT country_name FROM country";

    public abstract List<T> selectAll() throws DAOException;
    public abstract T select(K id) throws DAOException;
    public abstract boolean delete(K id) throws DAOException;
    public abstract boolean create(T entity) throws DAOException;
    public abstract boolean update(T entity) throws DAOException;

    String selectCountry(String countryName) throws DAOException {
        String idCountry = new String();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_COUNTRY);
            st.setString(1, countryName);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            idCountry = resultSet.getString("idcountry");
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return idCountry;
    }

    public List<String> selectAllCountry() throws DAOException {
        List<String> countryList = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_COUNTRY);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                countryList.add(resultSet.getString("country_name"));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return countryList;
    }

    void setStatement(PreparedStatement st, String... fields) throws SQLException {
        for (int i=0; i<fields.length; i++){
            st.setString(i+1, fields[i]);
        }
    }

    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            logger.error("Can't close statement!", e);
        }
    }

    public void close(ProxyConnection connection) throws DAOException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            logger.error("Can't return connection to pool", e);
        }
    }
}
