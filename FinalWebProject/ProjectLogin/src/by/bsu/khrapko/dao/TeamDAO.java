package by.bsu.khrapko.dao;

import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.domain.Team;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 07.04.2016.
 */
public class TeamDAO extends AbstractDAO<Long, Team> {
    private static final String SQL_SELECT_ALL_TEAM = "SELECT person_trip.idtrip, person.firstname, person.lastname, " +
            "person.position FROM person_trip JOIN person ON person_trip.idperson = person.idperson";
    private static final String SQL_SELECT_TEAM = "SELECT person.firstname, person.lastname, person.position FROM person " +
            "JOIN person_trip ON person.idperson = person_trip.idperson WHERE person_trip.idtrip = ? AND person.status = 1";
    private static final String SQL_DELETE_TEAM = "DELETE FROM person_trip WHERE idtrip = ?";
    private static final String SQL_UPDATE_TEAM = "INSERT INTO person_trip VALUES (?, ?)";


    @Override
    public List<Team> selectAll() throws DAOException{
        return null;
    }

    public HashMap<Long, Team> selectAllTeam() throws DAOException {
        HashMap<Long, Team> hashMap = new HashMap<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_TEAM);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()){
                if (!hashMap.containsKey(resultSet.getLong("person_trip.idtrip"))){
                    hashMap.put(resultSet.getLong("person_trip.idtrip"), new Team());
                }
                hashMap.get(resultSet.getLong("person_trip.idtrip")).setTeam(resultSet.getString("person.firstname")
                        + " " + resultSet.getString("person.lastname"), resultSet.getInt("person.position"));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        }
        return hashMap;
    }

    @Override
    public Team select(Long id) throws DAOException {
        Team team = new Team();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TEAM);
            st.setString(1, id.toString());
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()){
                team.setTeam(resultSet.getString("person.firstname") + " " + resultSet.getString("person.lastname"),
                        resultSet.getInt("person.position"));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return team;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_DELETE_TEAM);
            st.setString(1, Long.toString(id));
            st.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return true;
    }

    @Override
    public boolean create(Team entity) throws DAOException {
        return false;
    }

    @Override
    public boolean update(Team entity) throws DAOException {
        return false;
    }

    public boolean update(Long id, Team team) throws DAOException {
        delete(id);
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_UPDATE_TEAM);
            st.setString(2, id.toString());
            for (int i=0; i<4; i++) {
                if (team.getTeam(i+1) != null) {
                    st.setString(1, team.getTeam(i+1));
                    st.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return true;
    }
}
