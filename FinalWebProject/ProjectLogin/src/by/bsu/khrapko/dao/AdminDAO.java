package by.bsu.khrapko.dao;

import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.domain.Trip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 14.02.2016.
 */
public class AdminDAO extends AbstractDAO<Long,Trip> {
    static Logger logger = LogManager.getLogger(AdminDAO.class);

    private static final String SQL_SELECT_ALL_TRIP =
            "SELECT trip.idtrip, dep_country.country_name, country.country_name, trip.date_departure, " +
                    "trip.date_arrival, trip.duration, dep_country.timezone, country.timezone FROM trip " +
                    "JOIN country dep_country ON trip.country_departure = dep_country.idcountry " +
                    "JOIN country ON trip.country_arrival = country.idcountry ORDER BY trip.date_departure";
    private static final String SQL_INSERT_TRIP = "INSERT INTO trip (country_departure, country_arrival, " +
            "date_departure, date_arrival, duration) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_TRIP = "UPDATE trip SET country_departure = ?, country_arrival = ?, " +
            "date_departure = ?, date_arrival = ?, duration = ? WHERE idtrip = ?";
    private static final String SQL_SELECT_TRIP =
            "SElECT dep.country_name, country.country_name, trip.date_departure, trip.date_arrival, trip.duration, " +
                    "dep.timezone, country.timezone FROM trip JOIN country dep ON trip.country_departure = dep.idcountry " +
                    "JOIN country ON trip.country_arrival = country.idcountry WHERE idtrip = ? ";
    private static final String SQL_DELETE_TRIP = "DELETE FROM trip WHERE trip.idtrip = ?";
    private static final String SQL_SELECT_TRIP_BY_PERSON = "SELECT trip.idtrip, dep_country.country_name, country.country_name, " +
            "trip.date_departure, trip.date_arrival, trip.duration, dep_country.timezone, country.timezone FROM trip " +
            "JOIN country dep_country ON trip.country_departure = dep_country.idcountry " +
            "JOIN country ON trip.country_arrival = country.idcountry " +
            "JOIN person_trip ON trip.idtrip = person_trip.idtrip WHERE person_trip.idperson = ?";
    private static final String SQL_TEAM_FOR_TRIP = "SELECT COUNT(*) AS quantity FROM person_trip WHERE person_trip.idtrip = ?";

    @Override
    public List<Trip> selectAll() throws DAOException {
        List<Trip> tripList = new ArrayList<>();
        Trip trip;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_TRIP);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                trip = new Trip(resultSet.getLong("trip.idtrip"), resultSet.getString("dep_country.country_name"),
                        resultSet.getString("country.country_name"), resultSet.getString("trip.date_departure"),
                        resultSet.getString("trip.date_arrival"), resultSet.getString("trip.duration"),
                        resultSet.getDouble("dep_country.timezone"), resultSet.getDouble("country.timezone"));
                tripList.add(trip);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return tripList;
    }

    public List<Trip> selectTripByPerson(Long id) throws DAOException {
        List<Trip> tripList = new ArrayList<>();
        Trip trip;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TRIP_BY_PERSON);
            st.setString(1, id.toString());
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                trip = new Trip(resultSet.getLong("trip.idtrip"), resultSet.getString("dep_country.country_name"),
                        resultSet.getString("country.country_name"), resultSet.getString("trip.date_departure"),
                        resultSet.getString("trip.date_arrival"), resultSet.getString("trip.duration"),
                        resultSet.getDouble("dep_country.timezone"), resultSet.getDouble("country.timezone"));
                tripList.add(trip);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return tripList;
    }
    @Override
    public Trip select(Long id) throws DAOException {
        Trip trip = null;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_TRIP);
            st.setString(1, Long.toString(id));
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            trip = new Trip(id, resultSet.getString("dep.country_name"), resultSet.getString("country.country_name"),
                    resultSet.getString("trip.date_departure"), resultSet.getString("trip.date_arrival"),
                    resultSet.getString("trip.duration"), resultSet.getDouble("dep.timezone"), resultSet.getDouble("country.timezone"));
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return trip;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_DELETE_TRIP);
            st.setString(1, Long.toString(id));
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
            return false;
        }
    }

    @Override
    public boolean create(Trip trip) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_INSERT_TRIP);
            setStatement(st, selectCountry(trip.getCountryDeparture()), selectCountry(trip.getCountryArrival()),
                    trip.dateToString(trip.getDateDeparture()) + " " + trip.timeToString(trip.getDateDeparture()),
                    trip.dateToString(trip.getDateArrival()) + " " + trip.timeToString(trip.getDateArrival()),
                    trip.timeToString(trip.getDuration()));
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
    }

    @Override
    public boolean update(Trip trip) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_UPDATE_TRIP);
            setStatement(st, selectCountry(trip.getCountryDeparture()), selectCountry(trip.getCountryArrival()),
                    trip.dateToString(trip.getDateDeparture()) + " " + trip.timeToString(trip.getDateDeparture()),
                    trip.dateToString(trip.getDateArrival()) + " " + trip.timeToString(trip.getDateArrival()),
                    trip.timeToString(trip.getDuration()), trip.getId().toString());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
    }

    public boolean allowEdit(Long id) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_TEAM_FOR_TRIP);
            st.setString(1, Long.toString(id));
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            int num = resultSet.getInt("quantity");
            if (num > 0) {
                return false;
            }
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
    }
}
