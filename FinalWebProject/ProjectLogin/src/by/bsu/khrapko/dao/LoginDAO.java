package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.User;
import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 02.02.2016.
 */
public class LoginDAO extends AbstractDAO<String, User> {
    static Logger logger = LogManager.getLogger(LoginDAO.class);

    public static final String SQL_SELECT_USER_BY_LOGIN = "SELECT user.password, role.role_name " +
            "FROM user JOIN role ON user.role = role.idrole WHERE login=?";

    @Override
    public User select(String login) throws DAOException {
        User user = new User();
        if (Validator.validateLogin(login)) {
            ProxyConnection cn = null;
            PreparedStatement st = null;
            try {
                cn = ConnectionPool.getInstance().getConnection();
                st = cn.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
                st.setString(1, login);
                ResultSet resultSet = st.executeQuery();
                resultSet.next();
                user.setLogin(login);
                user.setPassword(resultSet.getString("user.password"));
                user.setRole(resultSet.getString("role.role_name"));
            } catch (SQLException e) {
                throw new DAOException("SQL exception (request or table failed): ", e);
            } finally {
                close(st);
                close(cn);
            }
        }
        return user;
    }

    @Override
    public List<User> selectAll() {
        return null;
    }

    @Override
    public boolean delete(String id) {
        return false;
    }

    @Override
    public boolean create(User entity) {
        return false;
    }

    @Override
    public boolean update(User entity) {
        return false;
    }

}
