package by.bsu.khrapko.dao;

import by.bsu.khrapko.dao.pool.ConnectionPool;
import by.bsu.khrapko.dao.pool.ProxyConnection;
import by.bsu.khrapko.domain.Person;
import by.bsu.khrapko.domain.Team;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by user on 26.02.2016.
 */
public class ManagerDAO extends AbstractDAO<Long, Person> {
    static Logger logger = LogManager.getLogger(ManagerDAO.class);

    private static final String SQL_SELECT_ALL_POSITION = "SELECT position_name FROM position";
    private static final String SQL_INSERT_PERSON = "INSERT INTO person (firstname, lastname, position, status, location) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_POSITION = "SELECT idposition FROM position WHERE position_name = ?";
    private static final String SQL_SELECT_ALL_PERSON = "SELECT person.idperson, person.firstname, person.lastname, " +
            "position.position_name, person.status, country.country_name FROM person " +
            "JOIN position ON person.position = position.idposition JOIN country ON person.location = country.idcountry";
    private static final String SQL_SELECT_PERSON = "SELECT person.firstname, person.lastname, position.position_name, " +
            "person.status, country.country_name FROM person JOIN position ON person.position = position.idposition " +
            "JOIN country ON person.location = country.idcountry WHERE person.idperson = ?";
    private static final String SQL_UPDATE_PERSON = "UPDATE person SET firstname = ?, lastname = ?, position = ?, " +
            "status = ?, location = ? WHERE idperson = ?";
    private static final String SQL_DELETE_PERSON = "DELETE FROM person WHERE person.idperson = ?";
    private static final String SQL_SELECT_ALL_PERSON_FOR_TRIP = "SELECT person.idperson, person.firstname, person.lastname, " +
            "position.position_name FROM person JOIN position ON person.position = position.idposition WHERE person.status = 1 " +
            "AND person.location = (SELECT trip.country_departure FROM trip WHERE trip.idtrip = ?)";
    private static final String SQL_TRIP_FOR_PERSON = "SELECT COUNT(*) AS quantity FROM person_trip WHERE person_trip.idperson = ?";

    @Override
    public List<Person> selectAll() throws DAOException {
        List<Person> personList = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_PERSON);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                personList.add(new Person(resultSet.getLong("person.idperson"),resultSet.getString("person.firstname"),
                        resultSet.getString("person.lastname"), resultSet.getString("position.position_name"),
                        resultSet.getString("country.country_name"), resultSet.getBoolean("person.status")));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return personList;
    }

    public List<Person> selectAll(Long tripId) throws DAOException {
        List<Person> personList = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_PERSON_FOR_TRIP);
            st.setString(1, tripId.toString());
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                personList.add(new Person(resultSet.getLong("person.idperson"),resultSet.getString("person.firstname"),
                        resultSet.getString("person.lastname"), resultSet.getString("position.position_name")));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return personList;
    }

    @Override
    public Person select(Long id) throws DAOException {
        Person p = null;
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_PERSON);
            st.setString(1, Long.toString(id));
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            p = new Person(resultSet.getString("person.firstname"), resultSet.getString("person.lastname"),
                    resultSet.getString("position.position_name"), resultSet.getString("country.country_name"),
                    resultSet.getInt("person.status"));
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return p;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_DELETE_PERSON);
            st.setString(1, Long.toString(id));
            st.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return true;
    }

    @Override
    public boolean create(Person p) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_INSERT_PERSON);
            setStatement(st, p.getFirstName(), p.getLastName(), selectPosition(p.getPosition()),
                    String.valueOf(p.getStatus() ? 1 : 0), selectCountry(p.getLocation()));
            st.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return true;
    }

    @Override
    public boolean update(Person p) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_UPDATE_PERSON);
            setStatement(st, p.getFirstName(), p.getLastName(), selectPosition(p.getPosition()),
                    String.valueOf(p.getStatus() ? 1 : 0), selectCountry(p.getLocation()), p.getId().toString());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return true;
    }

    public List<String> selectAllPosition() throws DAOException {
        List<String> positionList = new ArrayList<>();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_POSITION);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()) {
                positionList.add(resultSet.getString("position_name"));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return positionList;
    }

    private String selectPosition(String positionName) throws DAOException {
        String idPosition = new String();
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_SELECT_POSITION);
            st.setString(1, positionName);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            idPosition = resultSet.getString("idposition");
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
        return idPosition;
    }

    public boolean allowEdit(Long id) throws DAOException {
        ProxyConnection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().getConnection();
            st = cn.prepareStatement(SQL_TRIP_FOR_PERSON);
            st.setString(1, Long.toString(id));
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            int num = resultSet.getInt("quantity");
            if (num > 0) {
                return false;
            }
            return true;
        } catch (SQLException e) {
            throw new DAOException("SQL exception (request or table failed) ", e);
        } finally {
            close(st);
            close(cn);
        }
    }
}
