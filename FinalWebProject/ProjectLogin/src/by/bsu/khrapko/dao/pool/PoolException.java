package by.bsu.khrapko.dao.pool;
/**
 * Created by user on 31.01.2016.
 */
public class PoolException extends Exception {
    public PoolException() {
    }

    public PoolException(String s) {
    }

    public PoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public PoolException(Throwable cause) {
        super(cause);
    }

    public PoolException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
