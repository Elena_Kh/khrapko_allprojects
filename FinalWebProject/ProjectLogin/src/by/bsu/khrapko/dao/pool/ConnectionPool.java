package by.bsu.khrapko.dao.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.*;
import java.util.Iterator;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.*;

/**
 * Created by user on 31.01.2016.
 */
public class ConnectionPool{
    static Logger logger = LogManager.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private final static int POOL_SIZE = 5;
    private BlockingQueue<ProxyConnection> connections = new ArrayBlockingQueue<ProxyConnection>(POOL_SIZE);
    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean createPool = new AtomicBoolean(false), takeConnection = new AtomicBoolean(false);
    final static String DATABASE_URL = "jdbc:mysql://localhost/aviacompany?";

    private ConnectionPool(){
        int index = 0;
        final int MAX_INDEX = 25;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            logger.fatal("Can't register JDBC driver", e);
            throw new RuntimeException("Driver registration error!");
        }
        while ((connections.size() < POOL_SIZE) && (++index <= MAX_INDEX)){
            try {
                connections.add(new ProxyConnection(DriverManager.getConnection(DATABASE_URL, "root", "12345")));
            } catch (SQLException e) {
                logger.error("SQLException in creating connection", e);
            }
        }
        if (connections.isEmpty()){
            throw new RuntimeException("Connections were not created!");
        }
        logger.debug("connections were created");
        takeConnection.set(true);
    }

    @PostConstruct
    public static ConnectionPool getInstance () {
        if (!createPool.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    createPool.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public ProxyConnection getConnection() {
        if (takeConnection.get()) {
            try {
                return connections.take();
            } catch (InterruptedException e) {
               logger.error("Can't take connection!", e);
            }
        }
        return null;
    }

    public void returnConnection(ProxyConnection con) {
        try {
            if (!con.getAutoCommit()){
                con.setAutoCommit(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connections.offer(con);
        takeConnection.set(true);
    }

    @PreDestroy
    private void released(){
        takeConnection.set(false);
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException e) {
            logger.error("Problems in releasing", e);
        }
        Iterator iterator = connections.iterator();
        while (iterator.hasNext()) {
            ProxyConnection con = (ProxyConnection) iterator.next();
            con.closeConnection();
        }
        connections.clear();
    }
}
