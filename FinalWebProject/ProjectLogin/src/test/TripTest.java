package test;

import by.bsu.khrapko.domain.Trip;
import by.bsu.khrapko.service.AdminService;
import by.bsu.khrapko.service.ServiceException;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Created by user on 10.04.2016.
 */
public class TripTest {
    @Mock
    private AdminService adminService;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        adminService = new AdminService();
    }
        @Test
    public void shouldSelectTrip() throws ServiceException {
        Trip expectedTrip = new Trip("Minsk", "Oslo", "2016-05-13 18:20", "2016-05-14 15:30", "04:05");
            boolean result = adminService.insertTrip(expectedTrip);
            Assert.assertTrue("Problems in trip insert!", result);
        //Assert.assertEquals(expectedTrip, adminService.selectTrip(1L));
    }
}
