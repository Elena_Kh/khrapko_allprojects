package test;

import by.bsu.khrapko.service.ManagerService;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * Created by user on 09.04.2016.
 */
public class CountryTest {
    @Mock
    private ManagerService managerService;
    private Object[] countryList = new Object[19];
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        managerService = new ManagerService();
        countryList[0] = "Barcelona";
        countryList[1] = "Beijing";
        countryList[2] = "Berlin";
        countryList[3] = "Canberra";
        countryList[4] = "Dubai";
        countryList[5] = "Dublin";
        countryList[6] = "Helsinki";
        countryList[7] = "London";
        countryList[8] = "Los Angeles";
        countryList[9] = "Madrid";
        countryList[10] = "Milan";
        countryList[11] = "Minsk";
        countryList[12] = "Moscow";
        countryList[13] = "New York";
        countryList[14] = "Oslo";
        countryList[15] = "Ottawa";
        countryList[16] = "Rome";
        countryList[17] = "Stockholm";
        countryList[18] = "Tokyo";
    }
    @Test
    public void shouldCheckCountryList() throws ServiceException {
        Assert.assertArrayEquals(countryList, managerService.showCountryList().toArray());
    }
}
