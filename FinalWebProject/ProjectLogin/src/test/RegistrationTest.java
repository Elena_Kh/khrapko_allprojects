package test;

import by.bsu.khrapko.domain.User;
import by.bsu.khrapko.service.LoginService;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Created by user on 09.04.2016.
 */
public class RegistrationTest {
    @Mock
    private LoginService loginService;
    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        loginService = new LoginService();
    }
    @Test
    public void shouldCheckUser() throws ServiceException {
        boolean result = loginService.checkUser("admin");
        Assert.assertFalse(result);
    }

}
