<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 21.02.2016
  Time: 12:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <link rel="stylesheet" href="../css/header.css">
</head>
<body>
<div class="head">
    <ctg:info-user username="${userName}"></ctg:info-user>
    <a href="../controller?command=logout"><fmt:message key="logout"></fmt:message> </a>
</div>
</body>
</html>
