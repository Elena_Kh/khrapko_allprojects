<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 03.03.2016
  Time: 18:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Manager</title>
    <link rel="stylesheet" href="../css/manager.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/table.js"></script>
    <script type="text/javascript" src="../js/tabs.js"></script>
</head>
<body>
    <c:set var="pageName" value="managerEditPerson" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <jsp:include page="/jsp/header.jsp"/>
    <form id="editForm" name="editPersonForm" role="form" action="../controller">
        <div class="form-group">
        <input type="text" name="idPerson" value="${person.id}" hidden>
        <label class="control-label"><fmt:message key="firstname"/>*</label>
        <input class="form-control" type="text" name="firstname" value="${person.firstName}" required>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="lastname"/>*</label>
        <input class="form-control" type="text" name="lastname" value="${person.lastName}" required>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="position"/>*</label>
        <select class="form-control" name="position" size="1">
            <c:forEach var="position" items="${positionList}">
                <c:choose>
                    <c:when test="${position eq person.position}">
                        <option selected><c:out value="${position}"/></option>
                    </c:when>
                    <c:otherwise>
                        <option><c:out value="${position}"/></option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="location"/>*</label>
        <select class="form-control" name="location" size="1">
            <c:forEach var="country" items="${departureList}">
                <c:choose>
                    <c:when test="${country == person.location}">
                        <option selected><c:out value="${country}"/></option>
                    </c:when>
                    <c:otherwise>
                        <option><c:out value="${country}"/></option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="status"/></label>
        <select class="form-control" name="status" size="1">
            <c:choose>
                <c:when test="${person.status}">
                    <option value="active" selected><fmt:message key="status.active"></fmt:message> </option>
                    <option value="not active"><fmt:message key="status.notactive"></fmt:message> </option>
                </c:when>
                <c:otherwise>
                    <option value="active"><fmt:message key="status.active"></fmt:message> </option>
                    <option value="not active" selected><fmt:message key="status.notactive"></fmt:message> </option>
                </c:otherwise>
            </c:choose>
        </select>
        </div>
        <p class="help-block"> ${errorValidateMessage} </p>
        <div class="form-group">
        <button class="btn btn-default" type="submit" name="command" value="SavePerson">
            <fmt:message key="button.save"></fmt:message>
        </button>
        </div>
    </form>
    <br/>
    <form class="form-inline" action="../jsp/manager.jsp">
        <button class="btn btn-default" type="submit" name="command" value="AddPerson">
            <fmt:message key="button.addPerson"></fmt:message>
        </button>
    </form>
    <form action="../controller">
        <button class="btn btn-default" type="submit" name="command" value="ShowPersonList">
            <fmt:message key="button.showPersonList"></fmt:message>
        </button>
        <button class="btn btn-default" type="submit" name="command" value="ShowTripListWithTeam">
            <fmt:message key="button.showTripListWithTeam"></fmt:message>
        </button>
    </form>
</body>
</html>
