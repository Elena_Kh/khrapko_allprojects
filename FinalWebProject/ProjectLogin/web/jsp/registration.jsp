<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 25.02.2016
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="../css/login.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
</head>
<body>
    <c:set var="pageName" value="registration" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <div class="ref">
    <a name="entry" href="../jsp/login.jsp"><fmt:message key="entry"/></a>
    </div>
    <hr/>
    <div class="entry">
        <form id="entryForm" name="registrationForm" class="form-inline" role="form" method="POST" action="../controller">
            <div class="form-group">
                <label class="control-label"><fmt:message key="login"/>*</label><br>
                <input class="form-control" type="text" name="login" value="" pattern="\w{5,20}" title="5-20 символов" required/><br/>
            </div>
            <p></p>
            <div class="form-group">
                <label class="control-label"><fmt:message key="password"/>*</label><br/>
                <input class="form-control" type="password" name="password" value="" pattern="\w{5,20}" title="5-20 символов" required/><br/>
            </div>
            <p></p>
            <div class="form-group">
                <label class="control-label"><fmt:message key="position"/></label><br/>
                <select class="form-control" name="position" size="1">
                    <option value="admin" selected="selected"><fmt:message key="admin"></fmt:message> </option>
                    <option value="manager"><fmt:message key="manager"></fmt:message> </option>
                </select>
            </div>
            <p class="help-block"> ${errorRegistrMessage} </p>
            <div class="form-group">
                <button class="btn btn-default" type="submit" name="command" value="Registration">
                    <fmt:message key="button.registration"></fmt:message>
                </button>
            </div>
        </form>
    </div>
</body>
</html>
