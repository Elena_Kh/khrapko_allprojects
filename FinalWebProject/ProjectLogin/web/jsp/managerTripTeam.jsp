<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.02.2016
  Time: 12:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <link rel="stylesheet" href="../css/manager.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/table.js"></script>
</head>
<body>
    <c:set var="pageName" value="managerTripTeam" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <jsp:include page="/jsp/header.jsp"/>
    <form name="tripTeamListForm" action="../controller">
        <table class="paginated table" border="1" style="width:95%">
            <caption><fmt:message key="tripTeam"></fmt:message></caption>
            <thead>
            <tr>
                <th></th>
                <th><fmt:message key="countryDeparture"></fmt:message> </th>
                <th><fmt:message key="dateDeparture"></fmt:message> </th>
                <th><fmt:message key="duration"></fmt:message> </th>
                <th><fmt:message key="dateArrival"></fmt:message> </th>
                <th><fmt:message key="countryArrival"></fmt:message> </th>
                <th colspan="2"><fmt:message key="team"></fmt:message> </th>
            </tr>
            </thead>
            <tbody>
            <c:set var="trip" value="by.bsu.khrapko.domain.Trip"/>
            <c:forEach var="trip" items="${tripList}">
                <c:set var="team" value="${teamList.get(trip.id)}"/>
                <tr>
                    <td class="small" rowspan="2"><input type="radio" name="idTrip" value="${trip.id}"></td>
                    <td class="trans1"><c:out value="${trip.countryDeparture}"/></td>
                    <td class="trans1"><c:out value="${trip.dateToString(trip.dateDeparture)} ${trip.timeToString(trip.dateDeparture)}"/></td>
                    <td class="trans1"><c:out value="${trip.timeToString(trip.duration)}"/></td>
                    <td class="trans1"><c:out value="${trip.dateToString(trip.dateArrival(trip.dateDeparture, trip.duration))}
                                        ${trip.timeToString(trip.dateArrival(trip.dateDeparture, trip.duration))}"/></td>
                    <td class="trans1"><c:out value="${trip.countryArrival}"/></td>
                    <c:choose>
                        <c:when test="${not empty team.pilot}">
                            <td class="trans1"><fmt:message key="pilot"></fmt:message><c:out value=": ${team.pilot}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td class="trans1"><fmt:message key="pilot"></fmt:message><c:out value=": "/><fmt:message key="notSet"/></td>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${not empty team.navigator}">
                            <td class="trans-last1"><fmt:message key="navigator"></fmt:message> <c:out value=": ${team.navigator}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td class="trans-last1"><fmt:message key="navigator"></fmt:message> <c:out value=": "/><fmt:message key="notSet"/></td>
                        </c:otherwise>
                    </c:choose>
                </tr>
                <tr>
                    <td class="trans2"><c:out value="${trip.countryArrival}"/></td>
                    <td class="trans2"><c:out value="${trip.dateToString(trip.dateArrival)} ${trip.timeToString(trip.dateArrival)}"/></td>
                    <td class="trans2"><c:out value="${trip.timeToString(trip.duration)}"/></td>
                    <td class="trans2"><c:out value="${trip.dateToString(trip.dateArrival(trip.dateArrival, trip.duration))}
                                        ${trip.timeToString(trip.dateArrival(trip.dateArrival, trip.duration))}"/></td>
                    <td class="trans2"><c:out value="${trip.countryDeparture}"/></td>
                    <c:choose>
                        <c:when test="${not empty team.radioOperator}">
                            <td class="trans2"><fmt:message key="radioOperator"></fmt:message> <c:out value=": ${team.radioOperator}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td class="trans2"><fmt:message key="radioOperator"></fmt:message> <c:out value=": "/><fmt:message key="notSet"/></td>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${not empty team.stewardess}">
                            <td class="trans-last2"><fmt:message key="stewardess"></fmt:message> <c:out value=": ${team.stewardess}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td class="trans-last2"><fmt:message key="stewardess"></fmt:message> <c:out value=": "/><fmt:message key="notSet"/></td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <br/>

        <div class="center-block">
        <p class="help-block"> ${errorSelectMessage} </p>
        <button class="btn btn-default center" type="submit" name="command" value="EditTeam">
            <fmt:message key="button.editTeam"></fmt:message>
        </button>
        <button class="btn btn-default center" type="submit" name="command" value="ShowPersonList">
            <fmt:message key="button.showPersonList"></fmt:message>
        </button>
        </div>
    </form>
    <div class="center-block">
    <form action="../jsp/manager.jsp">
        <button class="btn btn-default center" type="submit" name="command" value="AddPerson">
            <fmt:message key="button.addPerson"></fmt:message>
        </button>
    </div>
    </form>
</body>
</html>
