<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 03.03.2016
  Time: 18:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Manager</title>
    <link rel="stylesheet" href="../css/manager.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>
    <c:set var="pageName" value="managerEditTeam" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <jsp:include page="/jsp/header.jsp"/>
    <h2>${tripDeparture}</h2>
    <h2>${tripArrival}</h2>
    <form id="editForm" name="editTeamForm" role="form" action="../controller">
        <%--<input type="text" name="idTrip" value="${idTrip}" hidden>--%>
        <div class="form-group">
        <label class="control-label"><fmt:message key="pilot"/></label>
        <select class="form-control" name="pilot" size="1">
            <c:forEach var="person" items="${personList}">
                <c:if test="${person.position eq 'pilot'}">
                    <c:choose>
                        <c:when test="${team.pilot eq person.toString()}">
                            <option value="${person.id}" selected><c:out value="${team.pilot}"/></option>
                        </c:when>
                        <c:otherwise>
                            <option value="${person.id}"><c:out value="${person.toString()}"/></option>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="navigator"/></label>
        <select class="form-control" name="navigator" size="1">
            <c:forEach var="person" items="${personList}">
                <c:if test="${person.position eq 'navigator'}">
                    <c:choose>
                        <c:when test="${team.navigator eq person.toString()}">
                            <option value="${person.id}" selected><c:out value="${team.navigator}"/></option>
                        </c:when>
                        <c:otherwise>
                            <option value="${person.id}"><c:out value="${person.toString()}"/></option>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="radioOperator"/></label>
        <select class="form-control" name="radioOperator" size="1">
            <c:forEach var="person" items="${personList}">
                <c:if test="${person.position eq 'radio operator'}">
                    <c:choose>
                        <c:when test="${team.radioOperator eq person.toString()}">
                            <option value="${person.id}" selected><c:out value="${team.radioOperator}"/></option>
                        </c:when>
                        <c:otherwise>
                            <option value="${person.id}"><c:out value="${person.toString()}"/></option>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="stewardess"/></label>
        <select class="form-control" name="stewardess" size="1">
            <c:forEach var="person" items="${personList}">
                <c:if test="${person.position eq 'stewardess'}">
                    <c:choose>
                        <c:when test="${team.stewardess eq person.toString()}">
                            <option value="${person.id}" selected><c:out value="${team.stewardess}"/></option>
                        </c:when>
                        <c:otherwise>
                            <option value="${person.id}"><c:out value="${person.toString()}"/></option>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:forEach>
        </select>
        </div>
        <p class="help-block"> ${errorSelectMessage}</p>
        <button class="btn btn-default" type="submit" name="command" value="SaveTeam">
            <fmt:message key="button.save"></fmt:message>
        </button>
    </form>

    <form action="../jsp/manager.jsp">
        <button class="btn btn-default" type="submit" name="command" value="AddPerson">
            <fmt:message key="button.addPerson"></fmt:message>
        </button>
    </form>
    <form action="../controller">
        <button class="btn btn-default" type="submit" name="command" value="ShowTripListWithTeam">
            <fmt:message key="button.showTripListWithTeam"></fmt:message>
        </button>
        <button class="btn btn-default" type="submit" name="command" value="ShowPersonList">
            <fmt:message key="button.showPersonList"></fmt:message>
        </button>
    </form>
</body>
</html>
