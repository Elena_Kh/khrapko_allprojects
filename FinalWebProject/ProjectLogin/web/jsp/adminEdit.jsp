<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 25.02.2016
  Time: 23:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="../css/admin.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
</head>
<body>
<c:set var="pageName" value="adminEdit" scope="session"></c:set>
<jsp:include page="/jsp/locale.jsp"></jsp:include>
<jsp:include page="/jsp/header.jsp"/>
<form role="form" id="addForm" name="addTripForm" action="../controller">
    <div class="form-group">
    <input type="text" name="idTrip" value="${idTrip}" hidden>
    <label class="control-label"><fmt:message key="countryDeparture"/></label>
    <select class="form-control" name="countryDeparture" size="1">
        <c:forEach var="country" items="${departureList}">
            <c:choose>
                <c:when test="${country == countryDeparture}">
                    <option selected><c:out value="${country}"/></option>
                </c:when>
                <c:otherwise>
                    <option><c:out value="${country}"/></option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>
    </div>
    <div class="form-group form-inline">
    <label class="control-label"><fmt:message key="dateDeparture"/>*</label>
    <input class="form-control" type="date" name="dateDeparture" value="${dateDeparture}" required>
    <input class="form-control" type="time" name="timeDeparture" value="${timeDeparture}" required>
    </div>
    <div class="form-group">
    <label class="control-label"><fmt:message key="duration"/>*</label>
    <input class="form-control" type="time" name="duration" value="${duration}" required/>
    </div>
    <div class="form-group form-inline">
    <label class="control-label"><fmt:message key="dateArrival"/>*</label>
    <input class="form-control" type="date" name="dateArrival" value="${dateArrival}" required>
    <input class="form-control" type="time" name="timeArrival" value="${timeArrival}" required>
    </div>
    <div class="form-group">
    <label class="control-label"><fmt:message key="countryArrival"/></label><br/>
    <select class="form-control" name="countryArrival" size="1">
        <c:forEach var="country" items="${arrivalList}">
            <c:choose>
                <c:when test="${country == countryArrival}">
                    <option selected><c:out value="${country}"/></option>
                </c:when>
                <c:otherwise>
                    <option><c:out value="${country}"/></option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>
    </div>
    <p class="help-block"> ${errorValidateMessage} </p>
    <div class="form-group">
    <button class="btn btn-default" type="submit" name="command" value="Edit">
        <fmt:message key="button.save"></fmt:message>
    </button>
    </div>
</form>
<br/>
<form action="../jsp/admin.jsp">
    <button class="btn btn-default" type="submit" name="command" value="AddTrip">
        <fmt:message key="button.addTrip"></fmt:message>
    </button>
</form>
<form class="form-inline" action="../jsp/adminList.jsp">
    <button class="btn btn-default" type="submit" name="command" value="ShowListTrip">
        <fmt:message key="button.showTripList"></fmt:message>
    </button>
</form>
</body>
</html>
