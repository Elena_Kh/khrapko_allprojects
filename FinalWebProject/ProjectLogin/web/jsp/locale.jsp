<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 26.03.2016
  Time: 18:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <link rel="stylesheet" href="../css/header.css">
    <script>
        function changeLocale(arg){
            var url;
            return url = location.href = "../controller?command=locale&loc=" + arg;
        }
    </script>
</head>
<body>
<div class="head">
   <label id="ru" onclick="changeLocale('ru')">RU</label>
    |
    <label id="en" onclick="changeLocale('en')">EN</label>
    |
    <label id="de" onclick="changeLocale('de')">DE</label>
</div>
</body>
</html>
