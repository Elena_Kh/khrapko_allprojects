<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 14.02.2016
  Time: 17:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Manager</title>
    <link rel="stylesheet" href="../css/manager.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/table.js"></script>
    <script type="text/javascript" src="js/tabs.js"></script>--%>
</head>
<body>
    <c:set var="pageName" value="manager" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <jsp:include page="/jsp/header.jsp"/>
    <form id="addForm" role="form" name="addPersonForm" action="../controller">
        <div class="form-group">
        <label class="control-label"><fmt:message key="firstname"/>*</label>
        <input class="form-control" type="text" name="firstname" value="" required>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="lastname"/>*</label>
        <input class="form-control" type="text" name="lastname" value="" required>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="position"/>*</label>
        <select class="form-control" name="position" size="1">
            <c:forEach var="position" items="${positionList}">
                <option><c:out value="${position}"/></option>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="location"/>*</label>
        <select class="form-control" name="location" size="1">
            <c:forEach var="location" items="${departureList}">
                <option><c:out value="${location}"/></option>
            </c:forEach>
        </select>
        </div>
        <div class="form-group">
        <label class="control-label"><fmt:message key="status"/></label>
        <select class="form-control" name="status" size="1">
            <option name="active" value="active"><fmt:message key="status.active"></fmt:message> </option>
            <option name="not active" value="not active"><fmt:message key="status.notactive"></fmt:message> </option>
        </select>
        </div>
        <p class="help-block"> ${errorValidateMessage} </p>
        <div class="form-group">
        <button class="btn btn-default" type="submit" name="command" value="AddPerson"><fmt:message key="button.add"></fmt:message> </button>
        </div>
    </form>
    <br/><br/>
    <form action="../controller">
        <button class="btn btn-default alone" type="submit" name="command" value="ShowPersonList">
            <fmt:message key="button.showPersonList"></fmt:message>
        </button>
        <button class="btn btn-default alone" type="submit" name="command" value="ShowTripListWithTeam">
            <fmt:message key="button.showTripListWithTeam"></fmt:message>
        </button>
    </form>
</body>
</html>
