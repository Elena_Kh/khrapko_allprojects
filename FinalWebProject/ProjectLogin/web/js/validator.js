/**
 * Created by user on 07.04.2016.
 */
$(document).ready(function(){
    $("#entryForm").validate({
        rules: {
            login:{
                required: true,
                minLength: 5,
                maxLength: 20,
            }
        }
    });
});
