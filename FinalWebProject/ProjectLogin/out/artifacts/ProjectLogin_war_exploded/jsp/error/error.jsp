<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 31.01.2016
  Time: 18:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Error page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/error.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
    <h4>Request from ${pageContext.errorData.requestURI} is failed</h4>
    <br/>
    <h4>Servlet name or type: ${pageContext.errorData.servletName}</h4>
    <br/>
    <h4>Status code: ${pageContext.errorData.statusCode}</h4>
    <br/>
    <h4>Exception: ${pageContext.errorData.throwable}</h4>
    <a href="../jsp/login.jsp"><fmt:message key="return"></fmt:message> </a>
</body>
</html>
