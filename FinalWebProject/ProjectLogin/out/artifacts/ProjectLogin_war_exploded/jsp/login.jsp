<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 31.01.2016
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="../css/login.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
</head>
<body>
    <c:set var="pageName" value="login" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <div class="ref">
    <a name="registration" href="../jsp/registration.jsp"><fmt:message key="registr"/></a>
    </div>
<hr/>
    <div class="entry">
        <form id="entryForm" name="loginForm" class="form-inline" role="form" method="POST" action="../controller">
            <div class="form-group">
                <label class="control-label"><fmt:message key="login"/>*</label><br/>
                <input class="form-control" type="text" name="login" value="" pattern="\w{5,20}" title="5-20 символов" required/><br/>
            </div>
            <p></p>
            <div class="form-group">
                <label class="control-label"><fmt:message key="password"/>*</label><br/>
                <input class="form-control" type="password" name="password" value="" pattern="\w{5,20}" title="5-20 символов" required/><br/>
            </div>
            <%--<c:if test="${not empty errorLoginPassMessage}">
                <p class="help-block"><fmt:message key="${errorLoginPassMessage}"></fmt:message></p>
            <%--</c:if>--%>
            <p class="help-block">${errorLoginPassMessage}</p>
            <%--    ${wrongAction}
            <br/>--%>
            <p class="help-block"> ${nullPage} </p>
            <br/>
            <div class="form-group">
                <button class="btn btn-default" type="submit" name="command" value="Login">
                    <fmt:message key="button.login"></fmt:message>
                </button>
            </div>
        </form>
    </div>
    <div id="footer">
        <ctg:info-time></ctg:info-time>
    </div>
</body>
</html>
