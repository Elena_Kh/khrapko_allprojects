<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 26.02.2016
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Person List</title>
    <link rel="stylesheet" href="../css/manager.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/tabs.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/table.js"></script>
    <script type="text/javascript" src="../js/tabs.js"></script>
</head>
<body>
<c:set var="pageName" value="managerPersonList" scope="session"></c:set>
<jsp:include page="/jsp/locale.jsp"></jsp:include>
<jsp:include page="/jsp/header.jsp"/>
<div class="wrapper">
    <div id="tabs">
        <ul id="items">
            <li class="active"><a href="#tabs-1"><fmt:message key="pilots"/></a></li>
            <li><a href="#tabs-2"><fmt:message key="navigators"/></a></li>
            <li><a href="#tabs-3"><fmt:message key="radioOperators"/></a></li>
            <li><a href="#tabs-4"><fmt:message key="stewardesses"/></a></li>
        </ul>
    </div>
    <div class="tab-content">
        <fmt:message key='firstname' var="first"></fmt:message>
        <fmt:message key='lastname' var="last"></fmt:message>
        <fmt:message key='location' var="loc"></fmt:message>
        <fmt:message key='status' var="sta"></fmt:message>
        <fmt:message key='status.active' var="act"></fmt:message>
        <fmt:message key='status.notactive' var="nact"></fmt:message>
        <div id="tabs-1" class="tabs-text">
            <form name="pilotsForm" action="../controller">
                <ctg:person-table position="pilot" firstname="${first}" lastname="${last}" location="${loc}" status="${sta}" active="${act}" notactive="${nact}"></ctg:person-table>
                <%--<table class="paginated table table-hover" border="1" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th><fmt:message key="firstname"></fmt:message> </th>
                        <th><fmt:message key="lastname"></fmt:message> </th>
                        <th><fmt:message key="location"></fmt:message> </th>
                        <th><fmt:message key="status"></fmt:message> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="p" value="by.bsu.khrapko.domain.Person"/>
                    <c:forEach var="p" items="${personList}">
                        <c:if test="${p.position eq 'pilot'}">
                            <tr>
                                <td><input type="radio" name="idPerson" value="${p.id}"></td>
                                <td><c:out value="${p.firstName}"/></td>
                                <td><c:out value="${p.lastName}"/></td>
                                <td><c:out value="${p.location}"/></td>
                                <c:choose>
                                    <c:when test="${p.status}">
                                        <td><fmt:message key="status.active"></fmt:message> </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:message key="status.notactive"></fmt:message> </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>--%>
                <div class="center-block">
                    <p class="help-block"> ${errorSelectMessage} </p>
                <button class="btn btn-default center" type="submit" name="command" value="EditPerson">
                    <fmt:message key="button.edit"></fmt:message>
                </button>
                <button class="btn btn-default center" type="submit" name="command" value="DeletePerson">
                    <fmt:message key="button.delete"></fmt:message>
                </button>
                </div>
            </form>
        </div>
        <div id="tabs-2" class="tabs-text">
            <form name="navigatorsForm" action="../controller">
                <ctg:person-table position="navigator" firstname="${first}" lastname="${last}" location="${loc}" status="${sta}" active="${act}" notactive="${nact}"></ctg:person-table>
                <%--<table class="paginated table table-hover" border="1" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th><fmt:message key="firstname"></fmt:message> </th>
                        <th><fmt:message key="lastname"></fmt:message> </th>
                        <th><fmt:message key="location"></fmt:message> </th>
                        <th><fmt:message key="status"></fmt:message> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="p" value="by.bsu.khrapko.domain.Person"/>
                    <c:forEach var="p" items="${personList}">
                        <c:if test="${p.position eq 'navigator'}">
                            <tr>
                                <td><input type="radio" name="idPerson" value="${p.id}"></td>
                                <td><c:out value="${p.firstName}"/></td>
                                <td><c:out value="${p.lastName}"/></td>
                                <td><c:out value="${p.location}"/></td>
                                <c:choose>
                                    <c:when test="${p.status}">
                                        <td><fmt:message key="status.active"></fmt:message> </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:message key="status.notactive"></fmt:message> </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>--%>
                <div class="center-block">
                    <p class="help-block"> ${errorSelectMessage} </p>
                <button class="btn btn-default center" type="submit" name="command" value="EditPerson">
                    <fmt:message key="button.edit"></fmt:message>
                </button>
                <button class="btn btn-default center" type="submit" name="command" value="DeletePerson">
                    <fmt:message key="button.delete"></fmt:message>
                </button>
                </div>
            </form>
        </div>
        <div id="tabs-3" class="tabs-text">
            <form name="radioOperatorsForm" action="../controller">
                <ctg:person-table position="radio operator" firstname="${first}" lastname="${last}" location="${loc}" status="${sta}" active="${act}" notactive="${nact}"></ctg:person-table>
                <%--<table class="paginated table table-hover" border="1" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th><fmt:message key="firstname"></fmt:message> </th>
                        <th><fmt:message key="lastname"></fmt:message> </th>
                        <th><fmt:message key="location"></fmt:message> </th>
                        <th><fmt:message key="status"></fmt:message> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="p" value="by.bsu.khrapko.domain.Person"/>
                    <c:forEach var="p" items="${personList}">
                        <c:if test="${p.position eq 'radio operator'}">
                            <tr>
                                <td><input type="radio" name="idPerson" value="${p.id}"></td>
                                <td><c:out value="${p.firstName}"/></td>
                                <td><c:out value="${p.lastName}"/></td>
                                <td><c:out value="${p.location}"/></td>
                                <c:choose>
                                    <c:when test="${p.status}">
                                        <td><fmt:message key="status.active"></fmt:message> </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:message key="status.notactive"></fmt:message> </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>--%>
                <div class="center-block">
                    <p class="help-block">${errorSelectMessage}</p>
                <button class="btn btn-default center" type="submit" name="command" value="EditPerson">
                    <fmt:message key="button.edit"></fmt:message>
                </button>
                <button class="btn btn-default center" type="submit" name="command" value="DeletePerson">
                    <fmt:message key="button.delete"></fmt:message>
                </button>
                </div>
            </form>
        </div>
        <div id="tabs-4" class="tabs-text">
            <form name="stewardessesForm" action="../controller">
                <ctg:person-table position="stewardess" firstname="${first}" lastname="${last}" location="${loc}" status="${sta}" active="${act}" notactive="${nact}"></ctg:person-table>
                <%--<table class="paginated table table-hover" border="1" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th><fmt:message key="firstname"></fmt:message> </th>
                        <th><fmt:message key="lastname"></fmt:message> </th>
                        <th><fmt:message key="location"></fmt:message> </th>
                        <th><fmt:message key="status"></fmt:message> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="p" value="by.bsu.khrapko.domain.Person"/>
                    <c:forEach var="p" items="${personList}">
                        <c:if test="${p.position eq 'stewardess'}">
                            <tr>
                                <td><input type="radio" name="idPerson" value="${p.id}"></td>
                                <td><c:out value="${p.firstName}"/></td>
                                <td><c:out value="${p.lastName}"/></td>
                                <td><c:out value="${p.location}"/></td>
                                <c:choose>
                                    <c:when test="${p.status}">
                                        <td><fmt:message key="status.active"></fmt:message> </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><fmt:message key="status.notactive"></fmt:message> </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:if>
                    </c:forEach>
                    </tbody>
                </table>--%>
                <div class="center-block">
                    <p class="help-block"> ${errorSelectMessage} </p>
                <button class="btn btn-default center" type="submit" name="command" value="EditPerson">
                    <fmt:message key="button.edit"></fmt:message>
                </button>
                <button class="btn btn-default center" type="submit" name="command" value="DeletePerson">
                    <fmt:message key="button.delete"></fmt:message>
                </button>
                </div>
            </form>
        </div>
    </div>
</div>
<br/>
<form action="../jsp/manager.jsp">
    <button class="btn btn-default" type="submit" name="command" value="AddPerson">
        <fmt:message key="button.addPerson"></fmt:message>
    </button>
</form>
<form action="../controller">
    <button class="btn btn-default" type="submit" name="command" value="ShowTripListWithTeam">
        <fmt:message key="button.showTripListWithTeam"></fmt:message>
    </button>
</form>
</body>
</html>
