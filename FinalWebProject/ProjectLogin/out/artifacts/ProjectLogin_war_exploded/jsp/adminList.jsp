<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 25.02.2016
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.names"/>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css" type='text/css' media='all'>
    <link rel="stylesheet" href="../css/admin.css">
    <link rel="stylesheet" href="../css/table.css">
    <script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/table.js"></script>
</head>
<body>
    <c:set var="pageName" value="adminList" scope="session"></c:set>
    <jsp:include page="/jsp/locale.jsp"></jsp:include>
    <jsp:include page="/jsp/header.jsp"/>
    <form name="tripListForm" action="../controller">
        <table class="paginated table" border="1" style="width:80%">
            <caption><fmt:message key="tripList"></fmt:message> </caption>
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col"><fmt:message key="countryDeparture"></fmt:message> </th>
                <th scope="col"><fmt:message key="dateDeparture"></fmt:message> </th>
                <th scope="col"><fmt:message key="duration"></fmt:message> </th>
                <th scope="col"><fmt:message key="dateArrival"></fmt:message> </th>
                <th scope="col"><fmt:message key="countryArrival"></fmt:message> </th>
            </tr>
            </thead>
            <tbody>
            <c:set var="trip" value="by.bsu.khrapko.domain.Trip"/>
            <c:forEach var="trip" items="${tripList}">
                <tr>
                    <td class="small" rowspan="2"><input type="radio" name="idTrip" value="${trip.id}"></td>
                    <td class="trans1"><c:out value="${trip.countryDeparture}"/></td>
                    <td class="trans1"><c:out value="${trip.dateToString(trip.dateDeparture)} ${trip.timeToString(trip.dateDeparture)}"/></td>
                    <td class="trans1"><c:out value="${trip.timeToString(trip.duration)}"/></td>
                    <td class="trans1"><c:out value="${trip.dateToString(trip.dateArrival(trip.dateDeparture, trip.duration))}
                                        ${trip.timeToString(trip.dateArrival(trip.dateDeparture, trip.duration))}"/></td>
                    <td class="trans-last1"><c:out value="${trip.countryArrival}"/></td>
                </tr>
                <tr>
                    <td class="trans2"><c:out value="${trip.countryArrival}"/></td>
                    <td class="trans2"><c:out value="${trip.dateToString(trip.dateArrival)} ${trip.timeToString(trip.dateArrival)}"/></td>
                    <td class="trans2"><c:out value="${trip.timeToString(trip.duration)}"/></td>
                    <td class="trans2"><c:out value="${trip.dateToString(trip.dateArrival(trip.dateArrival, trip.duration))}
                                        ${trip.timeToString(trip.dateArrival(trip.dateArrival, trip.duration))}"/></td>
                    <td class="trans-last2"><c:out value="${trip.countryDeparture}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="center-block">
            <p class="help-block"> ${errorSelectMessage} </p>
        <button class="btn btn-default center" type="submit" name="command" value="AddTrip">
            <fmt:message key="button.add"></fmt:message>
        </button>
        <button class="btn btn-default center" type="submit" name="command" value="EditTrip">
            <fmt:message key="button.edit"></fmt:message>
        </button>
        <button class="btn btn-default center" type="submit" name="command" value="DeleteTrip">
            <fmt:message key="button.delete"></fmt:message>
        </button>
        </div>
    </form>
    <br/>

</body>
</html>
