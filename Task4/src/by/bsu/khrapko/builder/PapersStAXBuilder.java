package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.PaperEnum;
import by.bsu.khrapko.entity.Chars;
import by.bsu.khrapko.entity.Magazine;
import by.bsu.khrapko.entity.Newspaper;
import by.bsu.khrapko.entity.Paper;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 13.01.2016.
 */
public class PapersStAXBuilder {
    private HashSet<Paper> papers = new HashSet<>();
    private XMLInputFactory inputFactory;
    public PapersStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }
    public Set<Paper> getPapers() {
        return papers;
    }

    public void buildSetPapers(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader = null;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);
            // StAX parsing
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.NEWSPAPER) {
                        Newspaper newspaper = buildNewspaper(reader);
                        papers.add(newspaper);
                    }else if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.MAGAZINE){
                        Magazine magazine = buildMagazine(reader);
                        papers.add(magazine);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            System.err.println("StAX parsing error! " + ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.err.println("File " + fileName + " not found! " + ex);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Impossible close file "+fileName+" : "+e);
            }
        }
    }

    /*private Paper buildPaper(XMLStreamException reader) throws XMLStreamException{
        Paper paper = new Paper();
        paper.setTitle(reader.getAttributeValue(null, PaperEnum.TITLE.getValue()));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PaperEnum.valueOf(name.toUpperCase())) {
                        case PERIODICITY:
                            paper.setPeriodicity(getXMLText(reader));
                            break;
                        case CHARS:
                            paper.setChars(getXMLChars(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.MAGAZINE) {
                        return paper;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Student");
    }*/

    private Magazine buildMagazine(XMLStreamReader reader) throws XMLStreamException{
        Magazine magazine = new Magazine();
        magazine.setTitle(reader.getAttributeValue(null, PaperEnum.TITLE.getValue()));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PaperEnum.valueOf(name.toUpperCase())) {
                        case PERIODICITY:
                            magazine.setPeriodicity(getXMLText(reader));
                            break;
                        case CHARS:
                            magazine.setChars(getXMLChars(reader));
                            break;
                        case GLOSS:
                            magazine.setGloss(Boolean.parseBoolean(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.MAGAZINE) {
                        return magazine;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Student");
    }

    private Newspaper buildNewspaper(XMLStreamReader reader) throws XMLStreamException {
        Newspaper newspaper = new Newspaper();
        newspaper.setTitle(reader.getAttributeValue(null, PaperEnum.TITLE.getValue()));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PaperEnum.valueOf(name.toUpperCase())) {
                        case PERIODICITY:
                            newspaper.setPeriodicity(getXMLText(reader));
                            break;
                        case CHARS:
                            newspaper.setChars(getXMLChars(reader));
                            break;
                        case PROGRAM:
                            newspaper.setProgram(Boolean.parseBoolean(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.NEWSPAPER) {
                        return newspaper;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Newspaper");
    }

    private Chars getXMLChars(XMLStreamReader reader) throws XMLStreamException {
        Chars chars = new Chars();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (PaperEnum.valueOf(name.toUpperCase())) {
                        case COLORED:
                            chars.setColored(Boolean.parseBoolean(getXMLText(reader)));
                            break;
                        case VOLUME:
                            chars.setVolume(Integer.parseInt(getXMLText(reader)));
                            break;
                        case SUBSCRIPTION:
                            chars.setSubscription(Integer.parseInt(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (PaperEnum.valueOf(name.toUpperCase()) == PaperEnum.CHARS){
                        return chars;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Address");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
