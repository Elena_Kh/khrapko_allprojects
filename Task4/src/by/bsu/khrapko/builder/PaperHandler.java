package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.PaperEnum;
import by.bsu.khrapko.entity.Magazine;
import by.bsu.khrapko.entity.Newspaper;
import by.bsu.khrapko.entity.Paper;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 03.01.2016.
 */
public class PaperHandler extends DefaultHandler {
    private Set<Paper> papers;
    private Paper current = null;
    private PaperEnum currentEnum = null;
    private EnumSet<PaperEnum> withInfo;

    static Logger logger = Logger.getLogger(PaperHandler.class);

    public PaperHandler() {
        papers = new HashSet<Paper>();
        withInfo = EnumSet.range(PaperEnum.COLORED, PaperEnum.GLOSS);
    }

    public Set<Paper> getPapers() {
        return papers;
    }

    @Override
    public void startDocument() {
        System.out.println("Parsing started");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        if ("newspaper".equals(localName)) {
            current = new Newspaper();
            current.setTitle(attrs.getValue(0));
            /*if (attrs.getLength() == 2) {
                current.setFaculty(attrs.getValue(1));
            }*/
        } else if ("magazine".equals(localName)) {
            current = new Magazine();
            current.setTitle(attrs.getValue(0));
        } else {
            PaperEnum temp = PaperEnum.valueOf(localName.toUpperCase());
            if (withInfo.contains(temp)) {
               currentEnum = temp;
            }
            //currentEnum = temp;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null){
            switch (currentEnum){
                case COLORED:
                    current.getChars().setColored(Boolean.parseBoolean(s));
                    break;
                case VOLUME:
                    current.getChars().setVolume(Integer.parseInt(s));
                    break;
                case SUBSCRIPTION:
                    current.getChars().setSubscription(Integer.parseInt(s));
                    break;
                case PERIODICITY:
                    current.setPeriodicity(s);
                    break;
                case PROGRAM:
                    ((Newspaper)current).setProgram(Boolean.parseBoolean(s));
                    break;
                case GLOSS:
                    ((Magazine)current).setGloss(Boolean.parseBoolean(s));
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ("newspaper".equals(localName) || "magazine".equals(localName)) {
            papers.add(current);
        }
    }

    @Override
    public void endDocument() {
        System.out.println("\nParsing ended");
    }
}
