package by.bsu.khrapko.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by user on 03.01.2016.
 */
public class SimpleMain {

    static{
        new DOMConfigurator().doConfigure("logs/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(SimpleMain.class);

    public static void main(String[ ] args) {
        PapersSAXBuilder saxBuilder = new PapersSAXBuilder();
        saxBuilder.buildSetPapers("data/papers.xml");
        System.out.println(saxBuilder.getPapers());

        PapersDOMBuilder domBuilder = new PapersDOMBuilder();
        domBuilder.buildSetPapers("data/papers.xml");
        System.out.println(domBuilder.getPapers());

        PapersStAXBuilder staxBuilder = new PapersStAXBuilder();
        staxBuilder.buildSetPapers("data/papers.xml");
        System.out.println(staxBuilder.getPapers());
    }
}
