package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.Paper;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

/**
 * Created by user on 03.01.2016.
 */
public class PapersSAXBuilder {

    private Set<Paper> papers;
    private PaperHandler sh;
    private XMLReader reader;

    static Logger logger = Logger.getLogger(PapersSAXBuilder.class);

    public PapersSAXBuilder() {
        // создание SAX-анализатора
        sh = new PaperHandler();
        try {
            // создание объекта-обработчика
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(sh);
        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: ", e);
        }
    }

    public Set<Paper> getPapers() {
        return papers;
    }

    public void buildSetPapers(String fileName) {
        try {
            // разбор XML-документа
            reader.parse(fileName);
        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: ", e);
        } catch (IOException e) {
            logger.error("ошибка I/О потока: ", e);
        }
        papers = sh.getPapers();
    }
}
