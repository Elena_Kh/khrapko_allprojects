package by.bsu.khrapko.entity;

import by.bsu.khrapko.entity.Chars;

/**
 * Created by user on 03.01.2016.
 */
public class Paper {
    private String title;
    private String periodicity;
    private Chars chars;

    public Paper(){
        title = new String();
        periodicity = new String();
        chars = new Chars();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return String.format("\nTitle: %s\nPeriodicity: %s\nChars:\n %s", title, periodicity, chars);
    }
}
