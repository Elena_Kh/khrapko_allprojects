package by.bsu.khrapko.entity;

/**
 * Created by user on 03.01.2016.
 */
public class Magazine extends Paper {
    private boolean gloss;

    public boolean isGloss() {
        return gloss;
    }

    public void setGloss(boolean gloss) {
        this.gloss = gloss;
    }

    @Override
    public String toString() {
        return super.toString()+String.format("Gloss: %b\n", gloss);
    }
}
