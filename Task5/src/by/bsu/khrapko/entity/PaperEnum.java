package by.bsu.khrapko.entity;

/**
 * Created by user on 03.01.2016.
 */
public enum PaperEnum {
    PAPERS("papers"),
    TITLE("title"),
    NEWSPAPER("newspaper"),
    MAGAZINE("magazine"),
    CHARS("chars"),
    COLORED("colored"),
    VOLUME("volume"),
    SUBSCRIPTION("subscription"),
    PERIODICITY("periodicity"),
    PROGRAM("program"),
    GLOSS("gloss");
    private String value;
    private PaperEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
