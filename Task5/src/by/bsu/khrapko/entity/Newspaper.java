package by.bsu.khrapko.entity;

/**
 * Created by user on 03.01.2016.
 */
public class Newspaper extends Paper {
    private boolean program;

    public boolean isProgram() {
        return program;
    }

    public void setProgram(boolean program) {
        this.program = program;
    }

    @Override
    public String toString() {
        return super.toString()+String.format("Program: %b\n", program);
    }
}
