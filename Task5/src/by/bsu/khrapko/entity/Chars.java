package by.bsu.khrapko.entity;

/**
 * Created by user on 03.01.2016.
 */
public class Chars {
    private Boolean colored;
    private Integer volume;
    private Integer subscription;

    public Boolean isColored() {
        return colored;
    }

    public void setColored(Boolean colored) {
        this.colored = colored;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getSubscription() {
        return subscription;
    }

    public void setSubscription(Integer subscription) {
        this.subscription = subscription;
    }

    @Override
    public String toString() {
        return String.format("\tColored: %b\n\tVolume: %d\n\tSubscription: %d\n", colored, volume, subscription);
    }
}
