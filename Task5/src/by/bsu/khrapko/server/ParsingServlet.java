package by.bsu.khrapko.server;

import by.bsu.khrapko.builder.PapersBuilder;
import by.bsu.khrapko.builder.PapersDOMBuilder;
import by.bsu.khrapko.builder.PapersSAXBuilder;
import by.bsu.khrapko.builder.PapersStAXBuilder;
import by.bsu.khrapko.validator.ValidatorSAX;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 24.01.2016.
 */
@WebServlet("/parsing")
public class ParsingServlet extends HttpServlet {

    static Logger log = LogManager.getLogger(ParsingServlet.class.getName());

    public ParsingServlet() {
        super();
    }

    public void init() throws ServletException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String method = request.getParameter("parser");
        String prefix = getServletContext().getRealPath("/");
        String filename = "data/papers.xml";
        ValidatorSAX validatorSAX = new ValidatorSAX();
        validatorSAX.validator(prefix);
        PapersBuilder builder;
        switch (method){
            case "sax":{
                log.debug("sax");
                builder = new PapersSAXBuilder();
                builder.buildSetPapers(prefix + filename);
                request.setAttribute("papers", builder.getPapers());
                break;
            }
            case "dom":{
                log.debug("dom");
                builder = new PapersDOMBuilder();
                builder.buildSetPapers(prefix + filename);
                request.setAttribute("papers", builder.getPapers());
                break;
            }
            case "stax":{
                log.debug("stax");
                builder = new PapersStAXBuilder();
                builder.buildSetPapers(prefix + filename);
                request.setAttribute("papers", builder.getPapers());
                break;
            }
        }
        request.setAttribute("method", method);
        request.getRequestDispatcher("/jsp/result.jsp").forward(request, response);
    }

    public void destroy() {
        super.destroy(); // Just puts "destroy" string in log
    }
}
