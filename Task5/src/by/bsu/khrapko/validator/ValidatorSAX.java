package by.bsu.khrapko.validator;

import by.bsu.khrapko.exception.PaperErrorHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by user on 12.01.2016.
 */
public class ValidatorSAX {

    static Logger logger = LogManager.getLogger(ValidatorSAX.class.getName());

    public void validator(String prefix) {
        Schema schema = null;
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        try {
            schema = factory.newSchema(new File(prefix + "data/papers.xsd"));
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setSchema(schema);
            SAXParser parser = spf.newSAXParser();
            parser.parse(prefix + "data/papers.xml", new PaperErrorHandler(logger));
            logger.info("papers.xml is valid");
        } catch (ParserConfigurationException e) {
            logger.error("papers.xml config error: ", e);
        } catch (SAXException e) {
            logger.error("papers.xml SAX error: ", e);
        } catch (IOException e) {
            logger.error("I/O error: ", e);
        }
    }
}
