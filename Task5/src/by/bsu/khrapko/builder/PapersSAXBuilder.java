package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.Paper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

/**
 * Created by user on 03.01.2016.
 */
public class PapersSAXBuilder extends PapersBuilder{

    static Logger logger = LogManager.getLogger(PapersSAXBuilder.class.getName());

    private PaperHandler sh;
    private XMLReader reader;

    public PapersSAXBuilder() {
        sh = new PaperHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(sh);
        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: ", e);
        }
    }

    public Set<Paper> getPapers() {
        return sh.getPapers();
    }

    public void buildSetPapers(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            logger.error("ошибка SAX парсера: ", e);
        } catch (IOException e) {
            logger.error("ошибка I/О потока: ", e);
        }
    }
}
