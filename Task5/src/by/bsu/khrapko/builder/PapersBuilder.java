package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.Paper;

import java.util.Set;

/**
 * Created by user on 26.01.2016.
 */
public abstract class PapersBuilder {
    abstract public void buildSetPapers(String filename);
    abstract public Set<Paper> getPapers();
}
