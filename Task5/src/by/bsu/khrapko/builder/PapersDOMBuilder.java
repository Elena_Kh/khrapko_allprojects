package by.bsu.khrapko.builder;

import by.bsu.khrapko.entity.PaperEnum;
import by.bsu.khrapko.entity.Chars;
import by.bsu.khrapko.entity.Magazine;
import by.bsu.khrapko.entity.Newspaper;
import by.bsu.khrapko.entity.Paper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 08.01.2016.
 */
public class PapersDOMBuilder extends PapersBuilder{

    static Logger logger = LogManager.getLogger(PapersDOMBuilder.class.getName());

    private Set<Paper> papers;
    private DocumentBuilder docBuilder;

    public PapersDOMBuilder() {
        this.papers = new HashSet<Paper>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("Ошибка конфигурации парсера: ", e);
        }
    }

    public Set<Paper> getPapers() {
        return papers;
    }

    public void buildSetPapers(String fileName) {
        try {
            Document doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList papersList = root.getElementsByTagName(PaperEnum.NEWSPAPER.getValue());
            for (int i = 0; i < papersList.getLength(); i++) {
                Element paperElement = (Element) papersList.item(i);
                Newspaper paper = buildNewspaper(paperElement);
                papers.add(paper);
            }
            papersList = root.getElementsByTagName(PaperEnum.MAGAZINE.getValue());
            for (int i = 0; i < papersList.getLength(); i++) {
                Element paperElement = (Element) papersList.item(i);
                Magazine paper = buildMagazine(paperElement);
                papers.add(paper);
            }
        } catch (IOException e) {
            logger.error("File error or I/O error: ", e);
        } catch (SAXException e) {
            logger.error("Parsing failure: ", e);
        }
    }

    private Newspaper buildNewspaper(Element paperElement) {
        Newspaper newspaper = new Newspaper();
        newspaper.setTitle(paperElement.getAttribute("title"));
        newspaper.setPeriodicity(getElementTextContent(paperElement, "periodicity"));
        Chars chars = new Chars();
        newspaper.setChars(chars);
        Element charsElement = (Element) paperElement.getElementsByTagName("chars").item(0);
        chars.setColored(Boolean.parseBoolean(getElementTextContent(charsElement, "colored")));
        chars.setVolume(Integer.parseInt(getElementTextContent(charsElement, "volume")));
        chars.setSubscription(Integer.parseInt(getElementTextContent(charsElement, "subscription")));
        newspaper.setProgram(Boolean.parseBoolean(getElementTextContent(paperElement, "program")));
        return newspaper;
    }

    private Magazine buildMagazine(Element paperElement){
        Magazine magazine = new Magazine();
        magazine.setTitle(paperElement.getAttribute("title"));
        magazine.setPeriodicity(getElementTextContent(paperElement, "periodicity"));
        Chars chars = new Chars();
        magazine.setChars(chars);
        Element charsElement = (Element) paperElement.getElementsByTagName("chars").item(0);
        chars.setColored(Boolean.parseBoolean(getElementTextContent(charsElement, "colored")));
        chars.setVolume(Integer.parseInt(getElementTextContent(charsElement, "volume")));
        chars.setSubscription(Integer.parseInt(getElementTextContent(charsElement, "subscription")));
        magazine.setGloss(Boolean.parseBoolean(getElementTextContent(paperElement, "gloss")));
        return magazine;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
