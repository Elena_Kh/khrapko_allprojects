<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 24.01.2016
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Result page</title>
</head>
<body>
    <p>Парсинг с помощью ${method.toUpperCase()}</p>
    <table border="1" style="width:100%">
        <caption>Newspapers</caption>
        <thead>
        <tr>
            <th>Название</th>
            <th>Периодичность</th>
            <th>Цветное</th>
            <th>Объем</th>
            <th>Подписной индекс</th>
            <th>Программа</th>
        </tr>
        </thead>
        <tbody>
        <c:set var="newspaper" value="by.bsu.khrapko.entity.Newspaper"/>
        <c:forEach var="paper" items="${papers}">
            <c:if test="${paper.getClass().name eq newspaper}">
                <tr>
                    <td><c:out value="${paper.title}"/></td>
                    <td><c:out value="${paper.periodicity}"/></td>
                    <td><c:out value="${paper.chars.isColored()}"/></td>
                    <td><c:out value="${paper.chars.volume}"/></td>
                    <td><c:out value="${paper.chars.subscription}"/></td>
                    <td><c:out value="${paper.program}"/></td>
                </tr>
            </c:if>
        </c:forEach>
        </tbody>
    </table>
    <p></p>
    <table border="2" style="width:100%">
        <caption>Magazines</caption>
        <thead>
        <tr>
            <th>Название</th>
            <th>Периодичность</th>
            <th>Цветное</th>
            <th>Объем</th>
            <th>Подписной индекс</th>
            <th>Глянцевое</th>
        </tr>
        </thead>
        <tbody>
            <c:set var="magazine" value="by.bsu.khrapko.entity.Magazine"/>
            <c:forEach var="paper" items="${papers}">
                <c:if test="${paper.getClass().name eq magazine}">
                    <tr>
                        <td><c:out value="${paper.title}"/></td>
                        <td><c:out value="${paper.periodicity}"/></td>
                        <td><c:out value="${paper.chars.isColored()}"/></td>
                        <td><c:out value="${paper.chars.volume}"/></td>
                        <td><c:out value="${paper.chars.subscription}"/></td>
                        <td><c:out value="${paper.gloss}"/></td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
    <p></p>
    <a href="../index.jsp">Go to start page</a>
</body>
</html>
