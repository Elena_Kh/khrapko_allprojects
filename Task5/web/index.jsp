<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 24.01.2016
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>JSP Parsing</title>
  </head>
  <body>
  <h5>Выбор метода парсинга документа</h5>
  <form name="Simple" action="parsing" method="POST">
      <select name="parser">
          <option value="sax">SAX</option>
          <option value="dom">DOM</option>
          <option value="stax">StAX</option>
      </select>
      <input type="submit" name="button" value="Парсинг"/>
  </form>
  </body>
</html>
